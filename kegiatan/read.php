<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../object/kegiatan.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
    $kegiatan = new Kegiatan($db);

    //query kegiatan
    $stmt = $kegiatan->read();
    $num = $stmt->rowCount();

    //check if more than 0 record found
    if($num > 0){
        //kegiatan array
        $kegiatan_arr = array();
        $kegiatan_arr["kegiatan"] = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);

            $kegiatan_item=array(
                "id_kegiatan" => $id_kegiatan,
                "kegiatan" => $kegiatan,
                "parent" => $parent
            );

            array_push($kegiatan_arr["kegiatan"], $kegiatan_item);
        }
    
        // set response code - 200 OK
        http_response_code(200);
    
        // show products data in json format
        echo json_encode($kegiatan_arr);
    }else{
 
        // set response code - 404 Not found
        http_response_code(404);
     
        // tell the user no products found
        echo json_encode(
            array("message" => "No file found.")
        );
    }
?>