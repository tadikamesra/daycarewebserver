-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 05, 2020 at 02:16 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id13055557_daycare`
--

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `jk` varchar(1) NOT NULL DEFAULT 'P' COMMENT 'P : Perempuan | L : Laki-laki',
  `telp` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `previlege` int(11) NOT NULL DEFAULT 0 COMMENT '1 untuk superuser',
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id_guru`, `nama`, `nip`, `email`, `jk`, `telp`, `password`, `previlege`, `create_at`, `last_update`) VALUES
(1, 'Fatimah', '2103171013', 'imaisnaini@gmail.com', 'P', '082245443941', 'd4950f0c012f857f596cde110a96fe82', 1, '2020-04-05 13:56:56', '2020-04-05 14:10:40'),
(2, 'Irma Dwi', '2103171015', 'iramdwim@gmail.com', 'P', '082139091573', '62358939d30e988a74a724bdcc67d69f', 0, '2020-04-05 13:56:56', '2020-04-05 13:56:56');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id_kegiatan` int(11) NOT NULL,
  `kegiatan` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id_kegiatan`, `kegiatan`, `parent`) VALUES
(1, 'Pemeriksaan Kondisi Tubuh', 0),
(2, 'Makan Pagi', 0),
(3, 'Kegiatan Pagi', 0),
(4, 'Sekolah', 3);

-- --------------------------------------------------------

--
-- Table structure for table `lingkup_perkembangan`
--

CREATE TABLE `lingkup_perkembangan` (
  `id_lingkup` int(11) NOT NULL,
  `lingkup` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT 0,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lingkup_perkembangan`
--

INSERT INTO `lingkup_perkembangan` (`id_lingkup`, `lingkup`, `parent`, `keterangan`) VALUES
(1, 'nilai agama dan moral', 0, ''),
(2, 'fisik motorik', 0, ''),
(3, 'kognitif', 0, ''),
(4, 'bahasa', 0, ''),
(5, 'sosial emosional', 0, ''),
(6, 'seni', 0, ''),
(7, 'motorik kasar', 2, ''),
(8, 'motorik halus', 2, ''),
(9, 'kesehatan dan perilaku keselamatan', 2, ''),
(10, 'mengenali lingkungan disekitarnya', 3, ''),
(11, 'menunjukan reaksi atas rangsangan', 3, ''),
(12, 'mengeluarkan suara untuk menyatakan keinginan atau sebagai reaksi atas stimulan', 4, ''),
(13, 'mampu membedakan antara bunyi dan suara', 6, ''),
(14, 'tertarik dengan suara atau musik', 6, ''),
(15, 'tertarik dengan berbagai macam karya seni', 6, ''),
(16, 'Belajar dan Pemecahan Masalah ', 3, ''),
(17, 'Berpikir Logis ', 3, ''),
(18, 'Berpikir Simbolik ', 3, ''),
(19, 'Memahami Bahasa ', 4, ''),
(20, 'Mengungkapkan Bahasa ', 4, ''),
(21, 'Anak mampu membedakan antara bunyi dan suara ', 6, ''),
(22, 'Tertarik dengan musik, lagu, atau nada bicara tertentu ', 6, ''),
(23, 'Tertarik  dengan karya seni  dan mencoba membuat suatu gerakan yang menimbulkan bunyi  ', 6, ''),
(24, 'Kesadaran Diri \r\n ', 5, ''),
(25, 'Tanggungjawab Diri dan Orang lain ', 5, ''),
(26, 'Perilaku Prososial ', 5, ''),
(27, 'Tertarik dengan kegiatan musik, gerakan orang, hewan maupun tumbuhan ', 6, ''),
(28, 'Tertarik dengan kegiatan atau karya seni ', 6, ''),
(29, 'Keaksaraan', 4, ''),
(30, 'Anak mampu menikmati berbagai alunan lagu atau suara ', 6, '');

-- --------------------------------------------------------

--
-- Table structure for table `murid`
--

CREATE TABLE `murid` (
  `id_murid` int(11) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` varchar(1) NOT NULL DEFAULT 'L' COMMENT 'P : Perempuan  | L : Laki-laki',
  `bb` int(11) NOT NULL,
  `tb` int(11) NOT NULL,
  `id_ortu` int(11) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `murid`
--

INSERT INTO `murid` (`id_murid`, `nik`, `nama`, `tgl_lahir`, `jk`, `bb`, `tb`, `id_ortu`, `create_at`, `last_update`) VALUES
(2, '358045304980003', 'Fatimah', '1998-04-13', 'P', 48, 150, 1, '2020-04-05 14:09:25', '2020-04-05 14:09:25'),
(3, '3578041909010002', 'Yazid', '2001-09-19', 'L', 52, 170, 1, '2020-04-05 14:09:25', '2020-04-05 14:09:25'),
(4, '3578041909010003', 'Syafiq', '2001-09-19', 'L', 50, 170, 1, '2020-04-05 14:09:25', '2020-04-05 14:09:25'),
(5, '3578041909010004', 'Dhani', '2014-04-15', 'L', 33, 115, 3, '2020-04-05 14:09:25', '2020-04-05 14:09:25'),
(6, '3578041909010006', 'Fajar', '2019-03-04', 'L', 19, 100, 2, '2020-04-05 14:09:25', '2020-04-05 14:09:25'),
(7, '3578041909010007', 'Rania', '2018-04-22', 'P', 17, 98, 2, '2020-04-05 14:09:25', '2020-04-05 14:09:25'),
(8, '3578041909010008', 'Budi', '2015-04-08', 'L', 20, 101, 3, '2020-04-05 14:09:25', '2020-04-05 14:09:25'),
(9, '3578041909010009', 'Salsa', '2016-09-16', 'P', 18, 100, 4, '2020-04-05 14:09:25', '2020-04-05 14:09:25'),
(10, '3578041909010010', 'Doni', '2017-04-14', 'L', 93, 103, 5, '2020-04-05 14:09:25', '2020-04-05 14:09:25'),
(11, '3578041909010011', 'Nana', '2018-09-11', 'P', 18, 102, 4, '2020-04-05 14:09:25', '2020-04-05 14:09:25');

-- --------------------------------------------------------

--
-- Table structure for table `orangtua`
--

CREATE TABLE `orangtua` (
  `id_ortu` int(11) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jk` varchar(1) NOT NULL DEFAULT 'P' COMMENT 'P : Perempuan | L : Laki-laki',
  `telp` varchar(15) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT current_timestamp(),
  `last_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orangtua`
--

INSERT INTO `orangtua` (`id_ortu`, `nik`, `nama`, `jk`, `telp`, `alamat`, `create_at`, `last_update`) VALUES
(1, '3578040105690006', 'Agus', 'L', '081230043114', 'Darmokali', '2020-04-05 14:09:52', '2020-04-05 14:09:52'),
(2, '3578040105690007', 'Ani', 'P', '081234676889', 'Sidoarjo', '2020-04-05 14:09:52', '2020-04-05 14:09:52'),
(3, '3578040105690008', 'Annisa', 'P', '085678778987', 'Surabaya', '2020-04-05 14:09:52', '2020-04-05 14:09:52'),
(4, '3578040105690009', 'Ahmad', 'L', '081334667889', 'Sidoarjo', '2020-04-05 14:09:52', '2020-04-05 14:09:52'),
(5, '3578040105690010', 'Salam', 'L', '087665778990', 'Surabaya', '2020-04-05 14:09:52', '2020-04-05 14:09:52');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id_report` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` time NOT NULL,
  `id_murid` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_kegiatan` int(10) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `id_tppa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tppa`
--

CREATE TABLE `tppa` (
  `id_tppa` int(11) NOT NULL,
  `usia` int(11) NOT NULL,
  `interval` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `id_lingkup` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tppa`
--

INSERT INTO `tppa` (`id_tppa`, `usia`, `interval`, `keterangan`, `id_lingkup`) VALUES
(1, 3, 0, 'Mendengar berbagai do’a, lagu religi, dan ucapan baik sesuai dengan agamanya ', 1),
(2, 6, 3, 'Melihat dan mendengar berbagai ciptaan Tuhan (makhluk hidup)', 1),
(3, 9, 3, 'Mengamati berbagai ciptaan Tuhan \r\n\r\n ', 1),
(4, 9, 3, 'Mendengarkan berbagai do’a, lagu religi, ucapan baik serta sebutan nama Tuhan ', 1),
(5, 12, 3, 'Mengamati kegiatan ibadah di sekitarnya ', 1),
(6, 3, 0, 'Berusaha mengangkat kepala saat ditelungkupkan ', 7),
(7, 3, 0, 'Menoleh ke kanan dan ke kiri ', 7),
(8, 3, 0, 'Berguling  (miring) ke kanan dan ke kiri ', 7),
(9, 6, 3, 'Tengkurap dengan dada diangkat dan kedua tangan menopang ', 7),
(10, 6, 3, 'Duduk dengan bantuan', 7),
(11, 6, 3, 'Mengangkat kedua kaki saat terlentang ', 7),
(12, 6, 3, 'Kepala tegak ketika duduk dengan bantuan', 7),
(13, 9, 3, 'Tengkurap bolakbalik tanpa bantuan', 7),
(14, 9, 3, 'Mengambil  benda yang  terjangkau ', 7),
(15, 9, 3, 'Memukul-mukulkan,melempar, atau menjatuhkan  benda yang dipegang ', 7),
(16, 9, 3, 'Merangkak ke segala arah ', 7),
(17, 9, 3, 'Duduk tanpa bantuan', 7),
(18, 9, 3, 'Berdiri berpegangan ', 7),
(19, 12, 3, 'Berjalan dengan berpegangan ', 7),
(20, 12, 3, 'Bertepuk tangan ', 7),
(21, 3, 0, 'Memiliki refleks menggenggam jari  ketika telapak tangannya disentuh ', 8),
(22, 3, 0, 'Memainkan jari tangan dan kaki ', 8),
(23, 3, 0, 'Memasukkan jari ke dalam mulut ', 8),
(24, 6, 3, 'Memegang benda dengan lima jari ', 8),
(25, 6, 3, 'Memainkan benda dengan tangan', 8),
(26, 6, 3, 'Meraih benda di depannya', 8),
(27, 9, 3, 'Memegang benda dengan ibu jari dan jari telunjuk (menjumput) ', 8),
(28, 9, 3, 'Meremas', 8),
(29, 9, 3, 'Memindahkan benda dari satu tangan ke tangan yang lain ', 8),
(30, 12, 3, 'Memasukkan benda ke mulut ', 8),
(31, 12, 3, 'Menggaruk kepala ', 8),
(32, 12, 3, 'Memegang benda kecil atau tipis (misal: potongan  buah atau biskuit)', 8),
(33, 12, 3, 'Memindahkan benda dari satu tangan ke tangan yang lain', 8),
(34, 3, 0, 'Berat badan sesuai tingkat usia', 9),
(35, 3, 0, 'Tinggi badan sesuai tingkat usia ', 9),
(36, 3, 0, 'Berat badan sesuai dengan standar tinggi badan', 9),
(37, 3, 0, 'Lingkar kepala sesuai tingkat usia', 9),
(38, 3, 0, 'Telah diimunisasi sesuai jadwal \r\n', 9),
(39, 6, 3, 'Berat badan sesuai  tingkat usia ', 9),
(40, 6, 3, 'Tinggi badan sesuai tingkat usia ', 9),
(41, 6, 3, 'Berat badan sesuai dengan standar tinggi badan ', 9),
(42, 6, 3, 'Lingkar kepala sesuai tingkat usia', 9),
(43, 6, 3, 'Telah diimunisasi sesuai jadwal ', 9),
(44, 6, 3, 'Bermain air ketika mandi ', 9),
(45, 6, 3, 'Merespon ketika lapar (misal, menangis, mencari puting susu ibu) ', 9),
(46, 6, 3, 'Menangis ketika mendengar suara keras ', 9),
(47, 9, 3, 'Berat badan sesuai tingkat usia', 9),
(48, 9, 3, 'Tinggi badan sesuai tingkat usia', 9),
(49, 9, 3, 'Berat badan sesuai dengan standar tinggi badan', 9),
(50, 9, 3, 'Lingkar kepala sesuai tingkat usia', 9),
(51, 9, 3, 'Telah diimunisasi sesuai jadwal ', 9),
(52, 9, 3, 'Menunjuk makanan yang diinginkan', 9),
(53, 9, 3, 'Mencari pengasuh atau orangtua', 9),
(54, 12, 3, 'Menjerit saat merasa tidak aman \r\n ', 9),
(55, 12, 3, 'Berat badan sesuai tingkat usia \r\n ', 9),
(56, 12, 3, 'Tinggi badan sesuai tingkat usia', 9),
(57, 12, 3, 'Berat badan sesuai dengan standar tinggi badan ', 9),
(58, 12, 3, 'Lingkar kepala sesuai tingkat usia ', 9),
(59, 12, 3, 'Menjerit saat merasa tidak aman ', 9),
(60, 12, 3, 'Menjerit saat merasa tidak aman ', 9),
(61, 3, 0, 'Mengenali wajah orang terdekat (ibu/ayah) ', 10),
(62, 3, 0, 'Mengenali suara orang terdekat (ibu/ayah) ', 10),
(63, 6, 3, 'Memperhatikan benda  yang ada di hadapannya', 10),
(64, 6, 3, 'Mendengarkan suara-suara di sekitarnya Ingin tahu lebih dalam dengan benda yang dipegangnya (misal: cara membongkar, membanting, dll) ', 10),
(65, 9, 3, 'Mengamati berbagai benda yang bergerak ', 10),
(66, 12, 3, 'Memahami perintah sederhana  ', 10),
(67, 3, 0, 'Memperhatikan benda bergerak atau suara/mainan yang menggantung  di atas tempat tidur ', 11),
(68, 6, 3, 'Mengulurkan kedua tangan untuk meminta (misal: digendong, dipangku, dipeluk) \r\n ', 11),
(69, 9, 3, 'Mengamati benda yang dipegang kemudian dijatuhkan', 11),
(70, 9, 3, 'Menjatuhkan benda yang dipegang secara berulang', 11),
(71, 9, 3, 'Berpaling ke arah sumber suara ', 11),
(72, 12, 3, 'Memberi reaksi menoleh  saat namanya dipanggil ', 11),
(73, 12, 3, 'Mencoba mencari benda yang disembunyikan ', 11),
(74, 12, 3, 'Mencoba membuka/ menutup gelas/cangkir  ', 11),
(75, 3, 0, 'Menangis', 4),
(76, 3, 0, 'Berteriak', 4),
(77, 3, 0, 'Bergumam', 4),
(78, 3, 0, 'Berhenti menangis setelah keinginannya terpenuhi (misal: setelah digendong atau diberi susu)', 4),
(79, 6, 3, 'Memperhatikan / mendengarkan ucapan orang', 4),
(80, 6, 3, 'Meraban atau berceloteh (babbling); seperti ba ba ba) ', 4),
(81, 6, 3, 'Tertawa kepada orang yang mengajak berkomunikasi ', 4),
(82, 9, 3, 'Mulai menirukan     kata yang terdiri dari dua suku kata ', 4),
(83, 9, 3, 'Merespon permainan “cilukba” ', 4),
(84, 12, 3, 'Menyatakan penolakan dengan menggeleng atau menangis', 4),
(85, 12, 3, 'Menunjuk benda yang diinginkan ', 4),
(86, 3, 0, 'Menatap dan tersenyum', 5),
(87, 3, 0, 'Menangis untuk mengekspresi kan ketidak nyamanan (misal, BAK, BAB, lingkungan panas) ', 5),
(88, 6, 3, 'Merespon dengan gerakan tangan dan kaki ', 5),
(89, 6, 3, 'Menangis apabila tidak mendapatkan yang diinginkan ', 5),
(90, 6, 3, 'Merespon dengan menangis/ menggerakkan tubuh pada orang yang belum dikenal ', 5),
(91, 9, 3, 'Menempelkan kepala bila merasa nyaman dalam pelukan (gendongan) atau meronta kalau merasa tidak nyaman ', 5),
(92, 12, 3, 'Menyatakan keinginan dengan berbagai gerakan tubuh dan ungkapan kata-kata sederhana ', 5),
(93, 12, 3, 'Meniru cara menyatakan perasaan (misal, cara memeluk, mencium) ', 5),
(94, 3, 0, 'Menoleh pada  berbagai suara musik atau bunyibunyian dengan irama teratur  ', 13),
(95, 6, 3, 'Mendengarkan  berbagai jenis musik atau bunyi-bunyian dengan irama yang teratur ', 13),
(96, 6, 3, 'Menjatuhkan benda untuk didengar suaranya ', 13),
(97, 9, 3, 'Melakukan tepuk tangan sederhana dengan irama tertentu ', 13),
(98, 9, 3, 'Tertarik dengan mainan yang mengeluarkan bunyi ', 13),
(99, 12, 3, 'Menggerakkan tubuh ketika mendengarkan musik ', 13),
(100, 12, 0, 'Memainkan alat permainan yang  mengeluarkan bunyi ', 13),
(101, 3, 0, ' Mendengar, menoleh, , atau memperhatikan   musik atau suara  dari pembicaraan orang tua/orang di sekitarnya ', 14),
(102, 3, 0, 'Melihat obyek yang diatasnya ', 14),
(103, 6, 3, 'Memperhatikan orang berbicara', 14),
(104, 6, 3, 'Memalingkan kepala mengikuti suara orang', 14),
(105, 6, 3, 'Memperhatikan jika didengarkan irama lagu dari mainan yang bersuara ', 14),
(106, 6, 3, 'Mengikuti irama lagu dengan suaranya secara sederhana ', 14),
(107, 6, 3, 'Mengamati obyek yang berbunyi  di sekitanya ', 14),
(108, 9, 3, 'Anak tertawa ketika diperlihatkan stimulus yang lucu/aneh', 14),
(109, 9, 3, 'Merespon bunyi atau suara dengan gerakan tubuh (misal: bergoyang-goyang) dengan ekspresi wajah yang sesuai ', 14),
(110, 12, 3, 'Memukul benda dengan irama teratur ', 14),
(111, 12, 3, 'Bersuara mengikuti irama musik atau lagu ', 14),
(112, 3, 0, 'Melihat ke gambar atau benda yang ditunjukkan 30 cm dari wajahnya ', 15),
(113, 6, 3, 'Menoleh atau memalingkan wajah secara spontan ketika ditunjukkan foto/ gambar/cermin dan berusaha menyentuh ', 15),
(114, 9, 3, 'Berusaha memegang benda, alat tulis yang diletakkan di hadapannya ', 15),
(115, 12, 3, 'Mencoret di atas media (misal: kertas, tembok) ', 15),
(116, 18, 6, 'Tertarik pada kegiatan ibadah (meniru gerakan ibadah, meniru bacaan do’a) ', 1),
(117, 24, 6, 'Menirukan gerakan ibadah dan doa ', 1),
(118, 24, 6, 'Mulai menunjukkan sikap-sikap baik (seperti yang diajarkan agama) terhadap orang yang sedang beribadah ', 1),
(119, 24, 6, 'Mengucapkan salam dan  kata-kata baik, seperti maaf, terima kasih pada situasi yang sesuai ', 1),
(120, 18, 6, 'Berjalan beberapa langkah tanpa bantuan ', 7),
(121, 18, 6, 'Naik turun tangga atau  tempat  yang lebih tinggi dengan merangkak ', 7),
(122, 18, 6, 'Dapat bangkit dari posisi duduk', 7),
(123, 18, 6, 'Melakukan gerak menendang bola ', 7),
(124, 18, 6, 'Berguling ke segala arah ', 7),
(125, 18, 6, 'Berjalan beberapa langkah tanpa bantuan ', 7),
(126, 24, 6, 'Berjalan sendiri tanpa jatuh ', 7),
(127, 24, 6, 'Melompat di tempat', 7),
(128, 24, 6, 'Naik turun tangga atau tempat yang lebih tinggi dengan bantuan ', 7),
(129, 24, 6, 'Berjalan mundur beberapa langkah ', 7),
(130, 24, 6, 'Menarik dan mendorong benda yang ringan (kursi kecil) ', 7),
(131, 24, 6, 'Melempar bola ke depan tanpa kehilangan keseimbangan ', 7),
(132, 24, 6, 'Menendang bola ke arah depan ', 7),
(133, 24, 6, 'Berdiri dengan satu kaki selama satu atau dua detik ', 7),
(134, 24, 6, 'Berjongkok ', 7),
(135, 18, 6, 'Membuat coretan bebas', 8),
(136, 18, 6, 'Menumpuk  tiga kubus ke atas', 8),
(137, 18, 6, 'Memegang gelas dengan dua tangan ', 8),
(138, 18, 6, 'Memasukkan  benda-benda ke dalam wadah ', 8),
(139, 18, 6, 'Menumpahkan benda-benda dari wadah ', 8),
(140, 24, 6, 'Membuat garis vertikal atau horizontal', 8),
(141, 24, 6, 'Membalik halaman buku walaupun belum sempurna ', 8),
(142, 24, 6, 'Menyobek kertas', 8),
(143, 18, 6, 'Berat badan sesuai standar usia 2', 9),
(144, 18, 6, 'Tinggi badan sesuai standar usia ', 9),
(145, 18, 6, 'Berat badan sesuai dengan standar tinggi badan ', 9),
(146, 18, 6, 'Mencuci tangan dengan bantuan ', 9),
(147, 18, 6, 'Merespon larangan orangtua namun masih memerlukan pengawasan dan bantuan ', 9),
(148, 24, 6, 'Berat badan sesuai standar usia ', 9),
(149, 24, 6, 'Tinggi badan sesuai standar usia ', 9),
(150, 24, 6, 'Berat badan sesuai dengan standar tinggi badan ', 9),
(151, 24, 6, 'Lingkar kepala sesuai standar pada usia ', 9),
(152, 24, 6, 'Mencuci tangan sendiri ', 9),
(153, 24, 6, 'Makan dengan sendok walau belum rapi ', 9),
(154, 24, 6, 'Menggosok gigi dengan bantuan ', 9),
(155, 24, 6, 'Memegang tangan orang dewasa ketika di tempat umum ', 9),
(156, 24, 6, 'Mengenal beberapa penanda rasa sakit (misal: menunjukkan rasa sakit pada bagian badan tertentu)  ', 9),
(157, 18, 6, 'Menyebut beberapa nama benda, jenis makanan ', 16),
(158, 18, 6, 'Menanyakan nama benda yang belum dikenal ', 16),
(159, 18, 6, 'Mengenal beberapa warna dasar (merah, biru, kuning, hijau)', 16),
(160, 18, 6, 'Menyebut nama sendiri dan orangorang yang dikenal ', 16),
(161, 24, 6, 'Mempergunakan alat permainan dengan cara memainkannya tidak beraturan,  seperti balok dipukul-pukul ', 16),
(162, 24, 6, 'Memahami  gambar wajah orang', 16),
(163, 24, 6, 'Memahami milik diri sendiri dan orang lain seperti: milik saya, milik kamu', 16),
(164, 24, 6, 'Menyebutkan berbagai nama makanan dan rasanya (misal,garam-asin, gula-manis) ', 16),
(167, 18, 6, ' Membedakan ukuran benda (besarkecil) ', 17),
(168, 18, 6, 'Membedakan penampilan yang rapi atau tidak ', 17),
(169, 18, 6, 'Merangkai puzzle sederhana \r\n ', 17),
(170, 24, 6, 'Menyusun balok dari besar ke kecil atau sebaliknya ', 17),
(171, 24, 6, 'Mengetahui akibat dari suatu perlakuannya (misal: menarik taplak meja akan menjatuhkan barang-barang di atasnya) ', 17),
(172, 24, 6, 'Merangkai puzzle ', 17),
(173, 18, 6, 'Menyebutkan bilangan tanpa menggunakan jari dari 1 -10 tetapi masih suka ada yang terlewat ', 18),
(174, 24, 6, 'Menyebutkan angka satu sampai lima dengan menggunakan jari ', 18),
(175, 18, 6, 'Menunjuk bagian tubuh yang ditanyakan ', 19),
(176, 18, 6, 'Memahami tema cerita yang didengar ', 19),
(177, 24, 6, 'Menaruh perhatian pada gambar-gambar dalam buku ', 19),
(178, 24, 6, 'Memahami  kata-kata sederhana dari ucapan yang didengar ', 19),
(179, 18, 6, 'Merespons pertanyaan dengan jawaban “Ya atau Tidak”  ', 20),
(180, 18, 6, 'Mengucapkan kalimat yang terdiri dari dua kata ', 20),
(181, 24, 6, 'Menjawab pertanyaan dengan  kalimat pendek', 20),
(182, 24, 6, 'Menyanyikan lagu sederhana', 20),
(183, 24, 6, 'Menyatakan keinginan dengan kalimat  pendek ', 20),
(184, 18, 6, 'Menunjukkan reaksi marah apabila merasa terganggu, seperti permainannya diambil ', 5),
(185, 18, 6, 'Menunjukkan reaksi yang berbeda terhadap orang yang baru dikenal ', 5),
(186, 18, 6, 'Bermain bersama teman tetapi sibuk dengan mainannya sendiri ', 5),
(187, 18, 6, 'Memperhatikan/mengamati temantemannya yang beraktivitas ', 5),
(188, 24, 6, 'Mengekspresikan berbagai reaksi emosi  (senang, marah, takut,  kecewa) ', 5),
(189, 24, 6, 'Menunjukkan reaksi menerima atau menolak kehadiran orang lain', 5),
(190, 24, 6, 'Bermain bersama teman dengan mainan yang sama ', 5),
(191, 24, 6, 'Meniru perilaku orang dewasa yang pernah dilihatnya ', 5),
(192, 24, 6, 'Makan dan minum sendir. \r\n', 5),
(193, 18, 6, 'Bisa menyanyikan lagu hanya kata terakhir (misalnya, “burung kakak .....” anak hanya menyebutkan kata “tua”) ', 21),
(194, 18, 6, 'Merespon berbagai macam suara orang terdekat, musik, atau lagu dengan menggoyangkan badan ', 21),
(195, 18, 6, 'Mengetahui suara binatang \r\n', 21),
(196, 18, 6, 'Paham adanya perbedaan suara/bahasa orang di sekitarnya (terutama ibu dan orang terdekatnya', 21),
(197, 24, 6, 'Anak mengenali musik dari program audio visual yang disukai (radio, TV, komputer, laptop) ', 21),
(198, 24, 6, 'Mendengar sesuatu dalam waktu yang lama ', 21),
(199, 24, 6, 'Secara berulang bermain dengan alat permainan yang mengeluarkan suara ', 21),
(200, 24, 6, 'Anak tertawa saat mendengar humor yang lucu ', 21),
(201, 18, 6, 'Menirukan bunyi, suara, atau musik dengan irama yang teratur', 22),
(202, 24, 6, 'Bertepuk tangan dan bergerak mengikuti irama dan birama ', 22),
(203, 24, 6, 'Bergumam lagu dengan 4 bait (misalnya, lagu balonku, bintang kecil, burung kakak tua) ', 22),
(204, 24, 6, 'Meniru suara binatang ', 22),
(205, 24, 6, 'Menunjukkan suatu reaksi kalau dilarang atau diperintah ', 22),
(206, 18, 6, ' Mencoret - coret ', 23),
(207, 18, 6, 'Mengusap dengan tangan pada kertas/kain dengan menggunakan berbagai media (misal, media bubur aci berwarna, cat air)  ', 23),
(208, 24, 6, 'Menggambar dari beberapa garis ', 23),
(209, 24, 6, 'Membentuk suatu karya  sederhana (berbentuk bulat atau lonjong) dari plastisin ', 23),
(210, 24, 6, 'Menyusun 4-6 balok membentuk suatu model', 23),
(211, 24, 6, 'Bertepuk tangan dengan pola sederhana', 23),
(212, 36, 12, 'Mulai meniru gerakan berdoa/sembahyang sesuai dengan agamanya', 1),
(213, 36, 12, 'Mulai memahami kapan mengucapkan salam, terima kasih, maaf, dsb', 1),
(214, 48, 12, 'Mengetahui  perilaku yang berlawanan meskipun belum selalu dilakukan seperti pemahaman perilaku baik-buruk, benar-salah, sopan-tidak sopan ', 1),
(215, 48, 12, 'Mengetahui  arti kasih dan sayang kepada ciptaan Tuhan ', 1),
(216, 48, 12, 'Mulai meniru doa pendek sesuai dengan agamanya \r\n ', 1),
(217, 36, 12, 'Berjalan sambil berjinjit ', 7),
(218, 36, 12, 'Melompat ke depan dan ke belakang dengan dua kaki ', 7),
(219, 36, 12, 'Melempar dan menangkap bola ', 7),
(220, 36, 12, 'Menari mengikuti irama', 7),
(221, 36, 12, 'Naik-turun tangga atau tempat yang lebih tinggi/rendah dengan berpegangan ', 7),
(222, 48, 12, 'Berlari sambil membawa sesuatu yang ringan (bola) ', 7),
(223, 48, 12, 'Naik-turun tangga atau tempat yang lebih tinggi dengan kaki bergantian ', 7),
(224, 48, 12, 'Meniti di atas papan yang cukup lebar', 7),
(225, 48, 12, 'Melompat turun dari ketinggian kurang lebih 20 cm (di bawah tinggi lutut anak)', 7),
(226, 48, 12, 'Meniru gerakan senam sederhana seperti menirukan gerakan pohon, kelinci melompat) ', 7),
(227, 48, 12, 'Berdiri dengan satu kaki ', 7),
(228, 36, 12, 'Meremas kertas atau kain dengan menggerakkan lima jari ', 8),
(229, 36, 12, 'Melipat kain/kertas meskipun belum rapi/lurus ', 8),
(230, 36, 12, 'Menggunting kertas tanpa pola ', 8),
(231, 36, 12, 'Koordinasi jari tangan cukup baik untuk memegang benda pipih seperti sikat gigi, sendok ', 8),
(232, 48, 12, 'Menuang air, pasir, atau biji-bijian ke dalam tempat penampung (mangkuk, ember) ', 8),
(234, 48, 12, 'Memasukkan benda kecil ke dalam botol (potongan lidi, kerikil, biji-bijian)', 8),
(235, 48, 12, 'Meronce benda yang cukup besar', 8),
(236, 48, 12, 'Menggunting kertas mengikuti pola garis lurus ', 8),
(237, 36, 12, 'Berat badan sesuai Tingkat usia ', 9),
(238, 36, 12, 'Tinggi badan sesuai Tingkat usia', 9),
(239, 36, 12, 'Berat badan sesuai dengan standar tinggi badan ', 9),
(240, 36, 12, 'Lingkar kepala sesuai Tingkat usia ', 9),
(241, 36, 12, 'Mencuci, membilas, dan mengelap  ketika cuci tangan tanpa bantuan', 9),
(242, 36, 12, 'Memberitahu orang dewasa bila sakit ', 9),
(243, 36, 12, 'Mencuci atau mengganti alat makan bila jatuh \r\n \r\n ', 9),
(244, 48, 12, 'Berat badan sesuai Tingkat usia', 9),
(245, 48, 12, 'Tinggi badan sesuai Tingkat usia', 9),
(246, 48, 12, 'Berat badan sesuai dengan standar tinggi badan ', 9),
(247, 48, 12, 'Lingkar kepala sesuai Tingkat usia ', 9),
(248, 48, 12, 'Membersihkan  kotoran  (ingus) ', 9),
(249, 48, 12, 'Menggosok  gigi ', 9),
(250, 48, 12, 'Memahami arti warna lampu lalu lintas ', 9),
(251, 48, 12, 'Mengelap tangan dan muka sendiri ', 9),
(252, 48, 12, 'Memahami kalau berjalan di sebelah kiri ', 9),
(253, 36, 12, 'Melihat dan menyentuh benda yang ditunjukkan oleh orang lain', 16),
(254, 36, 12, 'Meniru cara pemecahan orang dewasa atau teman ', 16),
(255, 36, 12, 'Konsentrasi dalam mengerjakan sesuatu tanpa bantuan orangtua', 16),
(256, 36, 12, 'Mengeksplorasi sebab dan akibat', 16),
(257, 36, 12, 'Mengikuti kebiasaan sehari-hari (mandi, makan, pergi ke sekolah) \r\n ', 16),
(258, 48, 12, 'Paham apabila ada bagian yang hilang dari suatu pola gambar seperti pada gambar wajah orang matanya tidak ada, mobil bannya copot, dsb', 16),
(259, 48, 12, 'Menyebutkan berbagai nama makanan dan rasanya (garam, gula atau cabai) ', 16),
(260, 48, 12, 'Menyebutkan berbagai macam kegunaan dari benda ', 16),
(261, 48, 12, 'Memahami persamaan antara dua benda ', 16),
(262, 48, 12, 'Memahami perbedaan antara dua hal dari jenis yang sama seperti membedakan antara buah rambutan dan pisang; perbedaan antara ayam dan kucing ', 16),
(263, 48, 12, 'Bereksperimen dengan bahan menggunakan cara baru ', 16),
(264, 48, 12, 'Mengerjakan tugas sampai selesai ', 16),
(265, 48, 12, 'Menjawab apa yang akan terjadi selanjutnya dari berbagai kemungkinan ', 16),
(266, 48, 12, 'Menyebutkan bilangan angka 1-10 ', 16),
(267, 48, 12, 'Mengenal beberapa huruf atau abjad tertentu dari A-z yang pernah dilihatnya ', 16),
(268, 36, 12, 'Menyebut bagian-bagian suatu gambar seperti gambar wajah orang, mobil, binatang, dsb', 17),
(269, 36, 12, 'Mengenal bagian-bagian tubuh (lima bagian)', 17),
(270, 36, 12, 'Memahami konsep ukuran (besarkecil, panjang-pendek) ', 17),
(271, 36, 12, 'Mengenal tiga macam bentuk lingkaran, segitiga, persegi', 17),
(272, 36, 12, 'Mulai mengenal pola', 17),
(273, 36, 12, 'Memahami simbol angka dan maknanya ', 17),
(274, 48, 12, 'Menempatkan benda dalam urutan ukuran (paling kecil-paling besar)', 17),
(275, 48, 12, 'Mulai mengikuti pola tepuk tangan', 17),
(276, 48, 12, 'Mengenal konsep banyak dan sedikit ', 17),
(277, 48, 12, 'Mengenali alasan mengapa ada sesuatu yang tidak masuk dalam kelompok tertentu ', 17),
(278, 48, 12, 'Menjelaskan model/karya yang dibuatnya ', 17),
(279, 36, 12, 'Meniru perilaku orang lain dalam   menggunakan barang ', 18),
(280, 36, 12, 'Memberikan nama atas karya yang dibuat ', 18),
(281, 36, 12, 'Melakukan aktivitas seperti kondisi nyata (misal: memegang gagang telpon) ', 18),
(282, 48, 12, 'Menyebutkan peran dan tugasnya (misal, koki tugasnya memasak) ', 18),
(283, 48, 12, 'Menggambar atau membentuk sesuatu konstruksi yang mendeskripsikan sesuatu yang spesifik ', 18),
(284, 48, 12, 'Melakukan aktivitas bersama teman dengan terencana (bermain berkelompok dengan memainkan peran tertentu seperti yang telah direncanakan) ', 18),
(285, 36, 12, 'Memainkan kata/suara yang\r\ndidengar dan diucapkan berulangulang', 19),
(286, 36, 12, 'Hafal beberapa lagu anak\r\nsederhana', 19),
(287, 36, 12, 'Hafal beberapa lagu anak\r\nsederhana', 19),
(288, 36, 12, 'Memahami perintah sederhana\r\nseperti letakkan mainan di atas\r\nmeja, ambil mainan dari dalam\r\nkotak', 19),
(289, 48, 12, 'Pura-pura membaca cerita bergambar dalam\r\nbuku dengan kata-kata sendiri', 19),
(290, 48, 12, 'Mulai memahami dua perintah yang diberikan\r\nbersamaan contoh: ambil mainan di atas meja\r\nlalu berikan kepada ibu pengasuh atau\r\npendidik', 19),
(291, 36, 12, 'Menggunakan kata tanya dengan\r\ntepat (apa, siapa, bagaimana,\r\nmengapa, dimana)', 20),
(292, 36, 12, 'Menggunakan 3 atau 4 kata untuk\r\nmemenuhi kebutuhannya (misal,\r\nmau minum air putih)\r\n', 20),
(293, 36, 12, 'Mulai menyatakan keinginan dengan\r\nmengucapkan kalimat sederhana (6 kata)', 20),
(294, 36, 12, 'Mulai menceritakan pengalaman yang dialami\r\ndengan cerita sederhana', 20),
(295, 36, 12, 'Memberi salam setiap mau pergi', 24),
(296, 36, 12, 'Memberi rekasi percaya pada orang\r\ndewasa', 24),
(297, 36, 12, 'Menyatakan perasaan terhadap\r\nanak lain\r\n', 24),
(298, 36, 12, 'Berbagi peran dalam suatu\r\npermainan (misal: menjadi dokter,\r\nperawat, pasien)\r\n', 24),
(299, 48, 12, 'Mengikuti aktivitas dalam suatu kegiatan\r\nbesar (misal: piknik)', 24),
(300, 48, 12, 'Meniru apa yang dilakukan orang dewasa', 24),
(301, 48, 12, 'Bereaksi terhadap hal-hal yang tidak benar\r\n(marah bila diganggu)', 24),
(302, 48, 12, 'Mengatakan perasaan secara verbal', 24),
(303, 36, 12, 'Mulai bisa mengungkapkan ketika\r\ningin buang air kecil dan buang air\r\nbesar', 25),
(304, 36, 12, 'Mulai memahami hak orang lain\r\n(harus antri, menunggu giliran.\r\n', 25),
(305, 36, 12, 'Mulai menunjukkan sikap berbagi,\r\nmembantu, bekerja bersam.', 25),
(306, 48, 12, 'Mulai bisa melakukan buang air kecil tanpa bantuan', 25),
(307, 48, 12, 'Bersabar menunggu giliran', 25),
(308, 48, 12, 'Mulai menunjukkan sikap toleran sehingga\r\ndapat bekerja dalam kelompok', 25),
(309, 48, 12, 'Mulai menghargai orang lain\r\n', 25),
(310, 48, 12, 'Mulai menunjukkan ekspresi menyesal ketika\r\nmelakukan kesalahan', 25),
(311, 36, 12, 'Bermain secara kooperatif dalam\r\nkelompok', 26),
(312, 36, 12, 'Peduli dengan orang lain\r\n(tersenyum, menanggapi bicara)', 26),
(313, 36, 12, 'Membagi pengalaman yang benar\r\ndan salah pada orang lain', 26),
(314, 36, 12, 'Bermain bersama berdasarkan\r\naturan tertentu ', 26),
(315, 48, 12, 'Membangun kerjasama\r\n', 26),
(316, 48, 12, 'Memahami adanya perbedaan perasaan\r\n(teman takut, saya tidak) ', 26),
(317, 48, 12, 'Meminjam dan meminjamkan mainan', 26),
(318, 36, 12, 'Memperhatikan dan mengenali suara\r\nyang bernyanyi atau berbicara', 21),
(319, 36, 12, 'Mengenali berbagai macam suara dari\r\nkendaraan', 21),
(320, 48, 12, 'Meminta untuk diperdengarkan lagu favorit secara berulang', 21),
(321, 36, 12, '. Menyanyi sampai tuntas\r\ndengan irama yang benar\r\n(nyanyian pendek atau 4 bait)', 27),
(322, 36, 12, 'Menyanyikan lebih dari 3 lagu\r\ndengan irama yang yang benar\r\nsampai tuntas (nyanyian pendek\r\natau 4 bait)\r\n', 27),
(323, 36, 12, 'Bersama teman-teman\r\nmenyanyikan lagu\r\n', 27),
(324, 36, 12, 'Bernyanyi mengikuti irama dengan\r\nbertepuk tangan atau\r\nmenghentakkan kaki', 27),
(325, 36, 12, 'Meniru gerakan berbagai binatang', 27),
(326, 36, 12, 'Paham bila orang terdekatnya (ibu)\r\nmenegur ', 27),
(327, 36, 12, 'Mencontoh gerakan orang lain', 27),
(328, 36, 12, 'Bertepuk tangan sesuai irama', 27),
(329, 48, 12, 'Mendengarkan atau menyanyikan lagu', 27),
(330, 48, 12, 'Menggerakkan tubuh sesuai irama\r\n', 27),
(331, 48, 12, 'Bertepuk tangan sesuai irama musik', 27),
(332, 48, 12, 'Meniru aktivitas orang baik secara langsung\r\nmaupun melalui media. (misal, cara\r\nminum/cara bicara/perilaku seperti ibu)\r\n', 27),
(333, 48, 12, 'Bertepuk tangan dengan pola yang berirama\r\n(misalnya bertepuk tangan sambil mengikuti\r\nirama nyanyian)', 27),
(334, 36, 12, 'Menggambar benda-benda lebih\r\nspesifik', 28),
(335, 36, 12, 'Mengamati dan membedakan benda\r\ndi sekitarnya yang di dalam rumah', 27),
(336, 48, 12, 'Menggambar dengan menggunakan beragam\r\nmedia (cat air, spidol, alat menggambar) dan\r\ncara (seperti finger painting, cat air, dll)\r\n', 28),
(337, 48, 12, 'Membentuk sesuatu dengan plastisin\r\n', 28),
(338, 48, 12, 'Mengamati dan membedakan benda di\r\nsekitarnya yang di luar rumah', 28),
(339, 60, 12, 'Mengetahui agama yang dianutnya', 1),
(340, 60, 12, 'Meniru gerakan beribadah dengan\r\nurutan yang benar', 1),
(341, 60, 12, 'Mengucapkan doa sebelum dan/atau\r\nsesudah melakukan sesuatu\r\n', 1),
(342, 60, 12, 'Mengenal perilaku baik/sopan dan\r\nburuk', 1),
(343, 60, 12, 'Membiasakan diri berperilaku baik', 1),
(344, 60, 12, 'Mengucapkan salam dan membalas\r\nsalam\r\n', 1),
(345, 72, 12, 'Mengenal agama yang dianut', 1),
(346, 72, 12, 'Mengerjakan ibadah\r\n', 1),
(347, 72, 12, 'Berperilaku jujur, penolong, sopan, hormat,\r\nsportif, dsb', 1),
(348, 72, 12, 'Menjaga kebersihan diri dan lingkungan \r\n', 1),
(349, 72, 12, 'Mengetahui hari besar agama\r\n', 1),
(350, 72, 12, 'Menghormati (toleransi) agama orang lain', 1),
(351, 60, 12, 'Menirukan gerakan binatang, pohon\r\ntertiup angin, pesawat terbang, dsb', 7),
(352, 60, 12, 'Melakukan gerakan menggantung\r\n(bergelayut)\r\n', 7),
(353, 60, 12, 'Melakukan gerakan melompat,\r\nmeloncat, dan berlari secara\r\nterkoordinasi', 7),
(354, 60, 12, 'Melempar sesuatu secara terarah', 7),
(355, 60, 12, 'Menangkap sesuatu secara tepat', 7),
(356, 60, 12, 'Melakukan gerakan antisipasi\r\n', 7),
(357, 60, 12, 'Menendang sesuatu secara terarah\r\n', 7),
(358, 60, 12, 'Memanfaatkan alat permainan di luar\r\nkelas', 7),
(359, 72, 12, 'Melakukan gerakan tubuh secara\r\nterkoordinasi untuk melatih kelenturan,\r\nkeseimbangan, dan kelincahan', 7),
(360, 72, 12, 'Melakukan koordinasi gerakan mata-kakitangan-kepala dalam menirukan tarian atau\r\nsenam', 7),
(361, 72, 12, 'Melakukan permainan fisik dengan aturan', 7),
(362, 72, 12, 'Terampil menggunakan tangan kanan dan\r\nkiri\r\n', 7),
(363, 72, 12, 'Melakukan kegiatan kebersihan diri', 7),
(364, 60, 12, 'Membuat garis vertikal, horizontal,\r\nlengkung kiri/kanan, miring\r\nkiri/kanan, dan lingkaran\r\n', 8),
(365, 60, 12, 'Menjiplak bentuk', 8),
(366, 60, 12, 'Mengkoordinasikan mata dan tangan\r\nuntuk melakukan gerakan yang rumit', 8),
(367, 60, 12, 'Melakukan gerakan manipulatif\r\nuntuk menghasilkan suatu bentuk\r\ndengan menggunakan berbagai media', 8),
(368, 60, 12, 'Mengekspresikan diri dengan\r\nberkarya seni menggunakan berbagai\r\nmedia', 8),
(369, 60, 12, 'Mengontrol gerakan tangan yang\r\nmeggunakan otot halus (menjumput,\r\nmengelus, mencolek, mengepal,\r\nmemelintir, memilin, memeras)', 8),
(370, 72, 12, 'Menggambar sesuai gagasannya', 8),
(371, 72, 12, 'Meniru bentuk', 8),
(372, 72, 12, 'Melakukan eksplorasi dengan berbagai\r\nmedia dan kegiatan\r\n', 8),
(373, 72, 12, 'Menggunakan alat tulis dan alat makan\r\ndengan benar', 8),
(374, 72, 12, 'Menggunting sesuai dengan pola', 8),
(375, 72, 12, 'Menempel gambar dengan tepat\r\n', 8),
(376, 72, 12, 'Mengekspresikan diri melalui gerakan\r\nmenggambar secara rinci', 8),
(378, 60, 12, 'Berat badan sesuai tingkat usia\r\n', 9),
(379, 60, 12, 'Tinggi badan sesuai tingkat usia', 9),
(380, 60, 12, 'Berat badan sesuai dengan standar\r\ntinggi badan', 9),
(381, 60, 12, 'Lingkar kepala sesuai tingkat usia ', 9),
(382, 60, 12, 'Menggunakan toilet (penggunaan air,\r\nmembersihkan diri) dengan bantuan\r\nminimal\r\n', 9),
(383, 60, 12, 'Memahami berbagai alarm bahaya\r\n(kebakaran, banjir, gempa)', 9),
(384, 60, 12, 'Mengenal rambu lalu lintas yang ada\r\ndi jalan', 9),
(385, 72, 12, 'Berat badan sesuai tingkat usia', 9),
(386, 72, 12, 'Tinggi badan sesuai standar usia', 9),
(387, 72, 12, 'Berat badan sesuai dengan standar tinggi\r\nbadan', 9),
(388, 72, 12, 'Lingkar kepala sesuai tingkat usia ', 9),
(389, 72, 12, 'Menutup hidung dan mulut (misal, ketika\r\nbatuk dan bersin)', 9),
(390, 72, 12, 'Membersihkan, dan membereskan tempat\r\nbermain\r\n', 9),
(391, 72, 12, 'Mengetahui situasi yang membahayakan\r\ndiri', 9),
(392, 72, 12, 'Memahami tata cara menyebrang', 9),
(393, 72, 12, 'Mengenal kebiasaan buruk bagi kesehatan\r\n(rokok, minuman keras)', 9),
(394, 60, 12, 'Mengenal benda berdasarkan fungsi\r\n(pisau untuk memotong, pensil untuk\r\nmenulis)', 16),
(395, 60, 12, 'Menggunakan benda-benda sebagai\r\npermain', 16),
(396, 60, 12, 'Mengenal konsep sederhana dalam\r\nkehidupan sehari-hari (gerimis, hujan,\r\ngelap, terang, temaram, dsb)', 16),
(397, 60, 12, 'Mengetahui konsep banyak dan\r\nsedikit', 16),
(398, 60, 12, 'Mengkreasikan sesuatu sesuai dengan\r\nidenya sendiri yang terkait dengan\r\nberbagai pemecahan masalah\r\n', 16),
(399, 60, 12, 'Mengamati benda dan gejala dengan\r\nrasa ingin tahu\r\n', 16),
(400, 60, 12, 'Mengenal pola kegiatan dan\r\nmenyadari pentingnya waktu', 16),
(401, 60, 12, 'Memahami posisi/kedudukan dalam\r\nkeluarga, ruang, lingkungan sosial\r\n(misal: sebagai peserta\r\ndidik/anak/teman)', 16),
(402, 72, 12, 'Menunjukkan aktivitas yang bersifat\r\neksploratif dan menyelidik (seperti: apa yang\r\nterjadi ketika air ditumpahkan)', 16),
(403, 72, 12, 'Memecahkan masalah sederhana dalam\r\nkehidupan sehari-hari dengan cara yang\r\nfleksibel dan diterima sosial', 16),
(404, 72, 12, 'Menerapkan pengetahuan atau pengalaman\r\ndalam konteks yang baru', 16),
(405, 72, 12, 'Menunjukkan sikap kreatif dalam\r\nmenyelesaikan masalah (ide, gagasan di\r\nluar kebiasaan)', 16),
(406, 60, 12, '. Mengklasifikasikan benda\r\nberdasarkan fungsi, bentuk atau\r\nwarna atau ukuran\r\n', 17),
(407, 60, 12, 'Mengenal gejala sebab-akibat yang\r\nterkait dengan dirinya', 17),
(408, 60, 12, 'Mengklasifikasikan benda ke dalam\r\nkelompok yang sama atau kelompok\r\nyang sejenis atau kelompok yang\r\nberpasangan dengan 2 variasi ', 17),
(409, 60, 12, 'Mengenal pola (misal, AB-AB dan\r\nABC-ABC) dan mengulanginya', 17),
(410, 60, 12, 'Mengurutkan benda berdasarkan 5\r\nseriasi ukuran atau warna', 17),
(411, 72, 12, 'Mengenal perbedaan berdasarkan ukuran:\r\n“lebih dari”; “kurang dari”; dan “paling/ter”', 17),
(412, 72, 12, 'Menunjukkan inisiatif dalam memilih tema\r\npermainan (seperti: ”ayo kita bermain\r\npura-pura seperti burung”)', 17),
(413, 72, 12, 'Menyusun perencanaan kegiatan yang akan\r\ndilakukan', 17),
(414, 72, 12, 'Mengenal sebab-akibat tentang\r\nlingkungannya (angin bertiupmenyebabkan\r\ndaun bergerak, air dapat menyebabkan\r\nsesuatu menjadi basah)\r\n', 17),
(415, 72, 12, 'Mengklasifikasikan benda berdasarkan\r\nwarna, bentuk, dan ukuran (3 variasi)\r\n', 17),
(416, 72, 12, 'Mengklasifikasikan benda yang lebih\r\nbanyak ke dalam kelompok yang sama atau\r\nkelompok yang sejenis, atau kelompok\r\nberpasangan yang lebih dari 2 variasi', 17),
(417, 72, 12, 'Mengenal pola ABCD-ABCD\r\n', 17),
(418, 72, 12, 'Mengurutkan benda berdasarkan ukuran\r\ndari paling kecil ke paling besar atau\r\nsebaliknya', 17),
(419, 60, 12, 'Membilang banyak benda satu sampai\r\nsepuluh\r\n', 18),
(420, 60, 12, 'Mengenal konsep bilangan\r\n', 18),
(421, 60, 12, 'Mengenal lambang bilangan\r\n', 18),
(422, 60, 12, 'Mengenal lambang huruf', 18),
(423, 72, 12, 'Menyebutkan lambang bilangan 1-10', 18),
(424, 72, 12, 'Menggunakan lambang bilangan untuk\r\nmenghitung', 18),
(425, 72, 12, 'Mencocokkan bilangan dengan lambang\r\nbilangan', 18),
(426, 72, 12, 'Mengenal berbagai macam lambang huruf\r\nvokal dan konsonan', 18),
(427, 72, 12, 'Merepresentasikan berbagai macam benda\r\ndalam bentuk gambar atau tulisan (ada\r\nbenda pensil yang diikuti tulisan dan\r\ngambar pensil)', 18),
(428, 60, 12, 'Menyimak perkataan orang lain\r\n(bahasa ibu atau bahasa lainnya)\r\n', 19),
(429, 60, 12, 'Mengerti dua perintah yang diberikan\r\nbersamaan\r\n', 19),
(430, 60, 12, 'Memahami cerita yang dibacakan\r\n', 19),
(431, 60, 12, 'Mengenal perbendaharaan kata\r\nmengenai kata sifat (nakal, pelit, baik\r\nhati, berani, baik, jelek, dsb)', 19),
(432, 60, 12, 'Mendengar dan membedakan bunyibunyian dalam Bahasa Indonesia\r\n(contoh, bunyi dan ucapan harus\r\nsama)\r\n', 19),
(433, 72, 12, 'Mengerti beberapa perintah secara\r\nbersamaan', 19),
(434, 72, 12, 'Mengulang kalimat yang lebih kompleks\r\n', 19),
(435, 72, 12, 'Memahami aturan dalam suatu permainan', 19),
(436, 72, 12, 'Senang dan menghargai bacaan', 19),
(437, 60, 12, 'Mengulang kalimat sederhana', 20),
(438, 60, 12, 'Bertanya dengan kalimat yang bena', 20),
(439, 60, 12, 'Menjawab pertanyaan sesuai\r\npertanyaan', 20),
(440, 60, 12, 'Mengungkapkan perasaan dengan\r\nkata sifat (baik, senang, nakal, pelit,\r\nbaik hati, berani, baik, jelek, dsb)', 20),
(441, 60, 12, 'Menyebutkan kata-kata yang dikenal ', 20),
(442, 60, 12, 'Mengutarakan pendapat kepada\r\norang lain', 20),
(443, 60, 12, '. Menyatakan alasan terhadap\r\nsesuatu yang diinginkan atau\r\nketidaksetujuan', 20),
(444, 60, 12, 'Menceritakan kembali\r\ncerita/dongeng yang pernah\r\ndidengar', 20),
(445, 60, 12, 'Memperkaya perbendaharaan kata', 20),
(446, 60, 12, 'Berpartisipasi dalam percakapan', 20),
(447, 72, 12, 'Menjawab pertanyaan yang lebih kompleks', 20),
(448, 72, 12, 'Menyebutkan kelompok gambar yang\r\nmemiliki bunyi yang sama', 20),
(449, 72, 12, 'Berkomunikasi secara lisan, memiliki\r\nperbendaharaan kata, serta mengenal\r\nsimbol-simbol untuk persiapan membaca,\r\nmenulis dan berhitung', 20),
(450, 72, 12, 'Menyusun kalimat sederhana dalam\r\nstruktur lengkap (pokok kalimat-predikatketerangan)\r\n', 20),
(451, 72, 12, 'Memiliki lebih banyak kata-kata untuk\r\nmengekpresikan ide pada orang lain', 20),
(452, 72, 12, 'Melanjutkan sebagian cerita/dongeng yang\r\ntelah diperdengarkan\r\n', 20),
(453, 72, 12, 'Menunjukkkan pemahaman konsep-konsep\r\ndalam buku cerita', 20),
(454, 60, 12, 'Mengenal simbol-simbol\r\n', 29),
(456, 60, 12, 'Mengenal suara–suara hewan/benda\r\nyang ada di sekitarnya', 29),
(457, 60, 12, 'Membuat coretan yang bermakna ', 29),
(458, 60, 12, 'Meniru (menuliskan dan\r\nmengucapkan) huruf A-Z', 29),
(459, 72, 12, 'Menyebutkan simbol-simbol huruf yang\r\ndikenal', 29),
(460, 72, 12, 'Mengenal suara huruf awal dari nama\r\nbenda-benda yang ada di sekitarnya\r\n', 29),
(461, 72, 12, 'Menyebutkan kelompok gambar yang\r\nmemiliki bunyi/huruf awal yang sama.', 29),
(462, 72, 12, 'Memahami hubungan antara bunyi dan\r\nbentuk huruf', 29),
(463, 72, 12, 'Membaca nama sendiri\r\n', 29),
(464, 72, 12, 'Menuliskan nama sendiri', 29),
(466, 72, 12, 'Memahami arti kata dalam cerita', 29),
(467, 60, 12, 'Menunjukkan sikap mandiri dalam\r\nmemilih kegiatan', 24),
(468, 60, 12, 'Mengendalikan perasaan', 24),
(469, 60, 12, 'Menunjukkan rasa percaya diri', 24),
(470, 60, 12, 'Memahami peraturan dan disiplin', 24),
(471, 60, 12, 'Memiliki sikap gigih (tidak mudah\r\nmenyerah)', 24),
(472, 60, 12, 'Bangga terhadap hasil karya sendiri', 24),
(473, 72, 12, 'Memperlihatkan kemampuan diri untuk\r\nmenyesuaikan dengan situasi ', 24),
(474, 72, 12, 'Memperlihatkan kehati-hatian kepada orang\r\nyang belum dikenal (menumbuhkan\r\nkepercayaan pada orang dewasa yang tepat)', 24),
(475, 72, 12, 'Mengenal perasaan sendiri dan\r\nmengelolanya secara wajar (mengendalikan\r\ndiri secara wajar)', 24),
(476, 60, 12, 'Menjaga diri sendiri dari\r\nlingkungannya', 25),
(477, 60, 12, 'Menghargai keunggulan orang lain\r\n', 25),
(478, 60, 12, 'Mau berbagi, menolong, dan\r\nmembantu teman', 25),
(479, 72, 12, 'Tahu akan hak nya', 25),
(480, 72, 12, 'Mentaati aturan kelas (kegiatan, aturan)', 25),
(481, 72, 12, 'Mengatur diri sendiri\r\n', 25),
(482, 72, 12, 'Bertanggung jawab atas perilakunya untuk\r\nkebaikan diri sendiri\r\n', 25),
(483, 60, 12, 'Menunjukan antusiasme dalam\r\nmelakukan permainan kompetitif\r\nsecara positif', 26),
(484, 60, 12, 'Menaati aturan yang berlaku dalam\r\nsuatu permainan', 26),
(485, 60, 12, 'Menghargai orang lain', 26),
(486, 60, 12, 'Menunjukkan rasa empati', 26),
(487, 72, 12, 'Bermain dengan teman sebaya', 26),
(488, 72, 12, 'Mengetahui perasaan temannya dan\r\nmerespon secara wajar', 26),
(489, 72, 12, 'Berbagi dengan orang lain', 26),
(490, 72, 12, 'Menghargai hak/pendapat/karya orang lain', 26),
(491, 72, 12, 'Menggunakan cara yang diterima secara\r\nsosial dalam menyelesaikan masalah\r\n(menggunakan fikiran untuk menyelesaikan\r\nmasalah)', 26),
(492, 72, 12, 'Bersikap kooperatif dengan teman\r\n', 26),
(493, 72, 12, 'Menunjukkan sikap toleran', 26),
(494, 72, 12, 'Mengekspresikan emosi yang sesuai dengan\r\nkondisi yang ada (senang-sedih-antusias\r\ndsb)', 26),
(495, 72, 12, 'Mengenal tata krama dan sopan santun\r\nsesuai dengan nilai sosial budaya setempat\r\n', 26),
(496, 60, 12, 'Senang mendengarkan berbagai\r\nmacam musik atau lagu kesukaannya', 30),
(497, 60, 12, 'Memainkan alat musik/instrumen/benda yang dapat membentuk irama yang teratur', 30),
(498, 72, 12, 'Anak bersenandung atau bernyanyi sambil\r\nmengerjakan sesuatu\r\n', 30),
(499, 72, 12, 'Memainkan alat musik/instrumen/benda\r\nbersama teman', 30),
(500, 60, 12, 'Memilih jenis lagu yang disukai\r\n', 28),
(501, 60, 12, 'Bernyanyi sendiriBernyanyi sendiri', 28),
(502, 60, 12, 'Menggunakan imajinasi untuk\r\nmencerminkan perasaan dalam\r\nsebuah peran', 28),
(503, 60, 12, 'Membedakan peran fantasi dan\r\nkenyataan\r\n', 28),
(504, 60, 12, 'Menggunakan dialog, perilaku, dan\r\nberbagai materi dalam menceritakan\r\nsuatu cerita\r\n', 28),
(505, 60, 12, 'Mengekspresikan gerakan dengan\r\nirama yang bervariasi', 28),
(506, 60, 12, 'Menggambar objek di sekitarnya', 28),
(507, 60, 12, 'Membentuk berdasarkan objek yang\r\ndilihatnya (mis. dengan plastisin,\r\ntanah liat)', 28),
(508, 60, 12, 'Mendeskripsikan sesuatu (seperti\r\nbinatang) dengan ekspresif yang\r\nberirama (contoh, anak menceritakan\r\ngajah dengan gerak dan mimik\r\ntertentu)', 28),
(509, 60, 12, 'Mengkombinasikan berbagai warna\r\nketika menggambar atau mewarnai\r\n', 28),
(510, 72, 12, 'Menyanyikan lagu dengan sikap yang benar', 28),
(511, 72, 12, 'Menggunakan berbagai macam alat musik\r\ntradisional maupun alat musik lain untuk\r\nmenirukan suatu irama atau lagu tertentu', 28),
(512, 72, 12, 'Bermain drama sederhana', 28),
(513, 72, 12, 'Menggambar berbagai macam bentuk yang\r\nberagam', 28),
(514, 72, 12, 'Melukis dengan berbagai cara dan objek', 28),
(515, 72, 12, 'Membuat karya seperti bentuk\r\nsesungguhnya dengan berbagai bahan\r\n(kertas, plastisin, balok, dll)', 28);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indexes for table `lingkup_perkembangan`
--
ALTER TABLE `lingkup_perkembangan`
  ADD PRIMARY KEY (`id_lingkup`);

--
-- Indexes for table `murid`
--
ALTER TABLE `murid`
  ADD PRIMARY KEY (`id_murid`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indexes for table `orangtua`
--
ALTER TABLE `orangtua`
  ADD PRIMARY KEY (`id_ortu`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id_report`),
  ADD KEY `report_ibfk_1` (`id_guru`),
  ADD KEY `report_ibfk_2` (`id_murid`),
  ADD KEY `report_ibfk_3` (`id_kegiatan`),
  ADD KEY `id_tppa` (`id_tppa`);

--
-- Indexes for table `tppa`
--
ALTER TABLE `tppa`
  ADD PRIMARY KEY (`id_tppa`),
  ADD KEY `tppa_ibfk_1` (`id_lingkup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lingkup_perkembangan`
--
ALTER TABLE `lingkup_perkembangan`
  MODIFY `id_lingkup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `murid`
--
ALTER TABLE `murid`
  MODIFY `id_murid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `orangtua`
--
ALTER TABLE `orangtua`
  MODIFY `id_ortu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id_report` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tppa`
--
ALTER TABLE `tppa`
  MODIFY `id_tppa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=516;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `report_ibfk_1` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id_guru`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `report_ibfk_2` FOREIGN KEY (`id_murid`) REFERENCES `murid` (`id_murid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `report_ibfk_3` FOREIGN KEY (`id_kegiatan`) REFERENCES `kegiatan` (`id_kegiatan`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `report_ibfk_4` FOREIGN KEY (`id_tppa`) REFERENCES `tppa` (`id_tppa`);

--
-- Constraints for table `tppa`
--
ALTER TABLE `tppa`
  ADD CONSTRAINT `tppa_ibfk_1` FOREIGN KEY (`id_lingkup`) REFERENCES `lingkup_perkembangan` (`id_lingkup`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
