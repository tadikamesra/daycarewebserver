<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../object/lingkup.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
    $lingkup = new Lingkup($db);

    //query lingkup
    $stmt = $lingkup->read();
    $num = $stmt->rowCount();

    //check if more than 0 record found
    if($num > 0){
        //lingkup array
        $lingkup_arr = array();
        $lingkup_arr["lingkup"] = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);

            $lingkup_item=array(
                "id_lingkup" => $id_lingkup,
                "lingkup" => ucfirst($lingkup),
                "parent" => $parent,
                "usia" => $usia,
                "interval" => $interval
            );

            array_push($lingkup_arr["lingkup"], $lingkup_item);
        }
    
        // set response code - 200 OK
        http_response_code(200);
    
        // show products data in json format
        echo json_encode($lingkup_arr);
    }else{
 
        // set response code - 404 Not found
        http_response_code(404);
     
        // tell the user no products found
        echo json_encode(
            array("message" => "No file found.")
        );
    }
?>