<?php
// required headers
    //header("Access-Control-Allow-Origin: http://localhost/EZPrint/admin/api/");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 

    // include database and object files
    include_once '../config/database.php';
    include_once '../object/report.php';

    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    date_default_timezone_set("Asia/Bangkok");

    // initialize object
    $report = new Report($db);
    $response = array();

    if  (isset($_POST['waktu']) 
        && isset($_POST['id_murid'])
        && isset($_POST['id_guru'])
        && isset($_POST['id_kegiatan'])
        && isset($_POST['keterangan'])) {
        $report->tanggal = date('Y-m-d');
        $report->waktu = htmlspecialchars($_POST['waktu']);
        $report->id_murid = htmlspecialchars($_POST['id_murid']);
        $report->id_guru = htmlspecialchars($_POST['id_guru']);
        $report->id_kegiatan = htmlspecialchars($_POST['id_kegiatan']);
        $report->keterangan = htmlspecialchars($_POST['keterangan']);

        if(isset($_POST['id_tppa'])){
            $report->id_tppa = htmlspecialchars($_POST['id_tppa']);
            $stmt = $report->addTPPA();
        }else{
            $stmt = $report->add();
        }

        if ($stmt) {
            $response['error'] = false;
            $response['message'] = 'Insert data success';
            // set response code - 200 OK
            http_response_code(200);    
            // show products data in json format
            echo json_encode($response);
        }else{
            // set response code - 502 Bad Gateway
            http_response_code(502);
            $response['error'] = true;
            $response['message'] = "Insert data failed";    
            // show products data in json format
            echo json_encode($response);
        }
        
    } else {
        // set response code - 400 Bad Request
        http_response_code(400);    
        $response['error'] = true;
        $response['message'] = 'Required parameters are not available';
        // show products data in json format
        echo json_encode($response);
    }
?>

