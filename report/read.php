<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../object/report.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
    $report = new Report($db);

    //query report
    $stmt = $report->read();
    $num = $stmt->rowCount();

    //check if more than 0 record found
    if($num > 0){
        //report array
        $report_arr = array();
        $report_arr["report"] = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);

            if(is_null($id_tppa)){
                $id_tppa = 0;
            }
            $report_item=array(
                "id_report" => $id_report,
                "tanggal" => $tanggal,
                "waktu" => $waktu,
                "id_murid" => $id_murid,
                "id_guru" => $id_guru,
                "id_kegiatan" => $id_kegiatan,
                "id_tppa" => $id_tppa,
                "keterangan" => $keterangan
            );

            array_push($report_arr["report"], $report_item);
        }
    
        // set response code - 200 OK
        http_response_code(200);
    
        // show products data in json format
        echo json_encode($report_arr);
    }else{
 
        // set response code - 404 Not found
        http_response_code(404);
     
        // tell the user no products found
        echo json_encode(
            array("message" => "No report found.")
        );
    }
?>