<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../object/murid.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
    $murid = new Murid($db);

    //query murid
    $stmt = $murid->list();
    $num = $stmt->rowCount();

    //check if more than 0 record found
    if($num > 0){
        //murid array
        $murid_arr = array();
        $murid_arr["murid"] = array();

        // retrieve our table contents
        // fetch() is faster than fetchAll()
        // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);
    
            $murid_item=array(
                "id_murid" => $id_murid,
                "nik" => $nik,
                "nama" => $nama,
                "tgl_lahir" => $tgl_lahir,
                "jk" => $jk,
                "bb" => $bb,
                "tb" => $tb,
                "id_ortu" => $id_ortu
            );
    
            array_push($murid_arr["murid"], $murid_item);
        }
    
        // set response code - 200 OK
        http_response_code(200);
    
        // show products data in json format
        echo json_encode($murid_arr);
    }else{
        // set response code - 404 Not found
        http_response_code(404);
     
        // tell the user no products found
        echo json_encode(
            array("message" => "No murid found.")
        );
    }
?>