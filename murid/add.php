<?php
// required headers
    //header("Access-Control-Allow-Origin: http://localhost/EZPrint/admin/api/");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 

    // include database and object files
    include_once '../config/database.php';
    include_once '../object/murid.php';

    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();

    // initialize object
    $murid = new Murid($db);
    $response = array();

    if (isset($_POST['nik']) 
        && isset($_POST['nama']) 
        && isset($_POST['id_ortu']) 
        && isset($_POST['tgl_lahir'])
        && isset($_POST['jk'])
        && isset($_POST['bb'])
        && isset($_POST['tb'])) {
        $murid->nik = htmlspecialchars($_POST['nik']);
        $murid->nama = htmlspecialchars($_POST['nama']);
        $murid->tgl_lahir = htmlspecialchars($_POST['tgl_lahir']);
        $murid->jk = htmlspecialchars($_POST['jk']);
        $murid->bb = htmlspecialchars($_POST['bb']);
        $murid->tb = htmlspecialchars($_POST['tb']);
        $murid->id_ortu = htmlspecialchars($_POST['id_ortu']);

        $stmt = $murid->add();

        if ($stmt) {
            $response['error'] = false;
            $response['message'] = 'Insert data success';
            // set response code - 200 OK
            http_response_code(200);    
            // show products data in json format
            echo json_encode($response);
        }else{
            // set response code - 502 Bad Gateway
            http_response_code(502);
            $response['error'] = true;
            $response['message'] = 'Insert data failed';    
            // show products data in json format
            echo json_encode($response);
        }
        
    } else {
        // set response code - 400 Bad Request
        http_response_code(400);    
        $response['error'] = true;
        $response['message'] = 'Required parameters are not available';
        // show products data in json format
        echo json_encode($response);
    }
?>