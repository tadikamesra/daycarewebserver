<?php
// required headers
    //header("Access-Control-Allow-Origin: http://localhost/EZPrint/admin/api/");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 

    // include database and object files
    include_once '../config/database.php';
    include_once '../objects/murid.php';

    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();

    // initialize object
    $murid = new Murid($db);

    if (isset($_POST['nik'])) {
        $murid->nik = htmlspecialchars($_POST['nik']);
    }

    $stmt = $murid->read();
    $num = $stmt->rowCount();
    $respons = array();

    if ($num > 0) {
        $respons['error'] = false;
        $respons['murid'] = array();

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);

            $murid_item = array(
                "id_murid" => $id_murid,
                "nama" => $nama,
                "tgl_lahir" => $tgl_lahir,
                "jk" => $jk,
                "bb" => $bb,
                "tb" => $tb,
                "id_ortu" => $id_ortu
            );

            array_push($respons['murid'], $murid_item);
        }
        // set response code - 200 OK
        http_response_code(200);
    
        // show products data in json format
        echo json_encode($respons);        
    } else {
        // set response code - 404 Not found
        http_response_code(404);
        
        // tell the user no products found
        $respons['error'] = true;
        $respons['message'] = "No alamat user found";
        echo json_encode($respons);
    }
?>