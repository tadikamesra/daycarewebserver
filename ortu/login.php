<?php
// required headers
    //header("Access-Control-Allow-Origin: http://localhost/EZPrint/admin/api/");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 

    // include database and object files
    include_once '../config/database.php';
    include_once '../object/ortu.php';

    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();

    // initialize object
    $ortu = new Ortu($db);
    $response = array();
    if(isset($_POST['nik']) && isset($_POST['telp'])){
        $ortu->nik = htmlspecialchars($_POST['nik']);
        $ortu->telp = htmlspecialchars($_POST['telp']);
        // Check jika email sudah terdaftar
        if($ortu->nikExists()){
            $stmt = $ortu->login();
            $num = $stmt->rowCount();

            //Check jika email dan telp cocok
            if($num > 0){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    
                    $ortu_arr["id_ortu"] = $id_ortu;
                    $ortu_arr["nama"] = $nama;
                    $ortu_arr["nik"] = $nik;
                    $ortu_arr["jk"] = $jk;
                    $ortu_arr["telp"] = $telp;
                    $ortu_arr["alamat"] = $alamat;
                    $ortu_arr["error"] = FALSE;
                    $ortu_arr["message"] = "Login sukses!";
                }
        
                // set response code - 200 OK
                http_response_code(200);
            
                // show products data in json format
                echo json_encode($ortu_arr);
            }else{
                // set response code - 403 Forbidden
                http_response_code(403);

                $ortu_arr["error"] = TRUE;
                $ortu_arr["message"] = "No Telfon salah!";
        
                // tell the ortu no products found
                echo json_encode($ortu_arr);    
            }
        }else{            
            // set response code - 404 Not found
            http_response_code(404);
            
            $ortu_arr["error"] = TRUE;
            $ortu_arr["message"] = "Nik belum terdaftar!";

            // tell the ortu no products found
            echo json_encode($ortu_arr);
        }
    }

?>