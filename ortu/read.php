<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../object/ortu.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
    $ortu = new Ortu($db);

    //query ortu
    $stmt = $ortu->read();
    $num = $stmt->rowCount();

    //check if more than 0 record found
    if($num > 0){
        //ortu array
        $ortu_arr = array();
        $ortu_arr["ortu"] = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);

            $ortu_item=array(
                "id_ortu" => $id_ortu,
                "nama" => $nama,
                "nik" => $nik,
                "jk" => $jk,
                "telp" => $telp,
                "alamat" => $alamat
            );

            array_push($ortu_arr["ortu"], $ortu_item);
        }
    
        // set response code - 200 OK
        http_response_code(200);
    
        // show products data in json format
        echo json_encode($ortu_arr);
    }else{
 
        // set response code - 404 Not found
        http_response_code(404);
     
        // tell the user no products found
        echo json_encode(
            array("message" => "No file found.")
        );
    }
?>