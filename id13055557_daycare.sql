-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2020 at 09:43 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id13055557_daycare`
--

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `jk` varchar(1) NOT NULL DEFAULT'P' COMMENT'P : Perempuan | L : Laki-laki',
  `telp` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `previllage` int(11) DEFAULT'0' COMMENT'1 untuk superuser'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id_guru`, `nama`, `nip`, `email`, `jk`, `telp`, `password`, `previllage`) VALUES
(1,'Fatimah Isnaini','2103171013','imaisnaini@gmail.com','P','082245443941','imaisnaini', 1),
(2,'Irma Dwi','2103171015','iramdwim@gmail.com','P','082139091573','62358939d30e988a74a724bdcc67d69f', 0),
(6,'Yasmin','2103171017','yasmin@gmail.com','P','082139091573','yasmin123', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id_kegiatan` int(11) NOT NULL,
  `kegiatan` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `lingkup_perkembangan`
--

CREATE TABLE `lingkup_perkembangan` (
  `id_lingkup` int(11) NOT NULL,
  `lingkup` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT'0',
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lingkup_perkembangan`
--

INSERT INTO `lingkup_perkembangan` (`id_lingkup`, `lingkup`, `parent`, `keterangan`) VALUES
(1,'nilai agama dan moral', 0,''),
(2,'fisik motorik', 0,''),
(3,'kognitif', 0,''),
(4,'bahasa', 0,''),
(5,'sosial emosional', 0,''),
(6,'seni', 0,''),
(7,'motorik kasar', 2,''),
(8,'motorik halus', 2,''),
(9,'kesehatan dan perilaku keselamatan', 2,''),
(10,'mengenali lingkungan disekitarnya', 3,''),
(11,'menunjukan reaksi atas rangsangan', 3,''),
(12,'mengeluarkan suara untuk menyatakan keinginan atau sebagai reaksi atas stimulan', 4,''),
(13,'mampu membedakan antara bunyi dan suara', 6,''),
(14,'tertarik dengan suara atau musik', 6,''),
(15,'tertarik dengan berbagai macam karya seni', 6,''),
(16,'Belajar dan Pemecahan Masalah', 3,''),
(17,'Berpikir Logis', 3,''),
(18,'Berpikir Simbolik', 3,''),
(19,'Memahami Bahasa', 4,''),
(20,'Mengungkapkan Bahasa', 4,''),
(21,'Anak mampu membedakan antara bunyi dan suara', 6,''),
(22,'Tertarik dengan musik, lagu, atau nada bicara tertentu', 6,''),
(23,'Tertarik  dengan karya seni  dan mencoba membuat suatu gerakan yang menimbulkan bunyi ', 6,''),
(24,'Kesadaran Diri ', 5,''),
(25,'Tanggungjawab Diri dan Orang lain', 5,''),
(26,'Perilaku Prososial', 5,''),
(27,'Tertarik dengan kegiatan musik, gerakan orang, hewan maupun tumbuhan', 6,''),
(28,'Tertarik dengan kegiatan atau karya seni', 6,''),
(29,'Keaksaraan', 4,''),
(30,'Anak mampu menikmati berbagai alunan lagu atau suara', 6,'');

-- --------------------------------------------------------

--
-- Table structure for table `murid`
--

CREATE TABLE `murid` (
  `id_murid` int(11) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jk` varchar(1) NOT NULL DEFAULT'L' COMMENT'P : Perempuan  | L : Laki-laki',
  `bb` int(11) NOT NULL,
  `tb` int(11) NOT NULL,
  `id_ortu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `murid`
--

INSERT INTO `murid` (`id_murid`, `nik`, `nama`, `tgl_lahir`, `jk`, `bb`, `tb`, `id_ortu`) VALUES
(2,'358045304980003','Fatimah','1998-04-13','P', 48, 150, 1),
(3,'3578041909010002','Yazid','2001-09-19','L', 52, 170, 1),
(4,'3578041909010003','Syafiq','2001-09-19','L', 50, 170, 1),
(5,'3578041909010004','Dhani','2014-04-15','L', 33, 115, 3),
(6,'3578041909010006','Fajar','2019-03-04','L', 19, 100, 2),
(7,'3578041909010007','Rania','2018-04-22','P', 17, 98, 2),
(8,'3578041909010008','Budi','2015-04-08','L', 20, 101, 3),
(9,'3578041909010009','Salsa','2016-09-16','P', 18, 100, 4),
(10,'3578041909010010','Doni','2017-04-14','L', 93, 103, 5),
(11,'3578041909010011','Nana','2018-09-11','P', 18, 102, 4);

-- --------------------------------------------------------

--
-- Table structure for table `orangtua`
--

CREATE TABLE `orangtua` (
  `id_ortu` int(11) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jk` varchar(1) NOT NULL DEFAULT'P' COMMENT'P : Perempuan | L : Laki-laki',
  `telp` varchar(15) NOT NULL,
  `alamat` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orangtua`
--

INSERT INTO `orangtua` (`id_ortu`, `nik`, `nama`, `jk`, `telp`, `alamat`) VALUES
(1,'3578040105690006','Agus','L','081230043114','Darmokali'),
(2,'3578040105690007','Ani','P','081234676889','Sidoarjo'),
(3,'3578040105690008','Annisa','P','085678778987','Surabaya'),
(4,'3578040105690009','Ahmad','L','081334667889','Sidoarjo'),
(5,'3578040105690010','Salam','L','087665778990','Surabaya');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id_report` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` time NOT NULL,
  `id_murid` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_kegiatan` int(10) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `id_tppa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tppa`
--

CREATE TABLE `tppa` (
  `id_tppa` int(11) NOT NULL,
  `usia` int(11) NOT NULL,
  `interval` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `id_lingkup` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tppa`
--

INSERT INTO `tppa` (`id_tppa`, `usia`, `interval`, `keterangan`, `id_lingkup`) VALUES
(1, 0, 0,'Mendengar berbagai do’a, lagu religi, dan ucapan baik sesuai dengan agamanya', 1),
(2, 3, 3,'Melihat dan mendengar berbagai ciptaan Tuhan (makhluk hidup)', 1),
(3, 6, 3,'Mengamati berbagai ciptaan Tuhan', 1),
(4, 6, 3,'Mendengarkan berbagai do’a, lagu religi, ucapan baik serta sebutan nama Tuhan', 1),
(5, 9, 3,'Mengamati kegiatan ibadah di sekitarnya', 1),
(6, 0, 0,'Berusaha mengangkat kepala saat ditelungkupkan', 7),
(7, 0, 0,'Menoleh ke kanan dan ke kiri', 7),
(8, 0, 0,'Berguling  (miring) ke kanan dan ke kiri', 7),
(9, 3, 3,'Tengkurap dengan dada diangkat dan kedua tangan menopang', 7),
(10, 3, 3,'Duduk dengan bantuan', 7),
(11, 3, 3,'Mengangkat kedua kaki saat terlentang', 7),
(12, 3, 3,'Kepala tegak ketika duduk dengan bantuan', 7),
(13, 6, 3,'Tengkurap bolakbalik tanpa bantuan', 7),
(14, 6, 3,'Mengambil  benda yang  terjangkau', 7),
(15, 6, 3,'Memukul-mukulkan,melempar, atau menjatuhkan  benda yang dipegang', 7),
(16, 6, 3,'Merangkak ke segala arah', 7),
(17, 6, 3,'Duduk tanpa bantuan', 7),
(18, 6, 3,'Berdiri berpegangan', 7),
(19, 9, 3,'Berjalan dengan berpegangan', 7),
(20, 9, 3,'Bertepuk tangan', 7),
(21, 0, 0,'Memiliki refleks menggenggam jari  ketika telapak tangannya disentuh', 8),
(22, 0, 0,'Memainkan jari tangan dan kaki', 8),
(23, 0, 0,'Memasukkan jari ke dalam mulut', 8),
(24, 3, 3,'Memegang benda dengan lima jari', 8),
(25, 3, 3,'Memainkan benda dengan tangan', 8),
(26, 3, 3,'Meraih benda di depannya', 8),
(27, 6, 3,'Memegang benda dengan ibu jari dan jari telunjuk (menjumput)', 8),
(28, 6, 3,'Meremas', 8),
(29, 6, 3,'Memindahkan benda dari satu tangan ke tangan yang lain', 8),
(30, 9, 3,'Memasukkan benda ke mulut', 8),
(31, 9, 3,'Menggaruk kepala', 8),
(32, 9, 3,'Memegang benda kecil atau tipis (misal: potongan  buah atau biskuit)', 8),
(33, 9, 3,'Memindahkan benda dari satu tangan ke tangan yang lain', 8),
(34, 0, 0,'Berat badan sesuai tingkat usia', 9),
(35, 0, 0,'Tinggi badan sesuai tingkat usia', 9),
(36, 0, 0,'Berat badan sesuai dengan standar tinggi badan', 9),
(37, 0, 0,'Lingkar kepala sesuai tingkat usia', 9),
(38, 0, 0,'Telah diimunisasi sesuai jadwal', 9),
(39, 3, 3,'Berat badan sesuai  tingkat usia', 9),
(40, 3, 3,'Tinggi badan sesuai tingkat usia', 9),
(41, 3, 3,'Berat badan sesuai dengan standar tinggi badan', 9),
(42, 3, 3,'Lingkar kepala sesuai tingkat usia', 9),
(43, 3, 3,'Telah diimunisasi sesuai jadwal', 9),
(44, 3, 3,'Bermain air ketika mandi', 9),
(45, 3, 3,'Merespon ketika lapar (misal, menangis, mencari puting susu ibu)', 9),
(46, 3, 3,'Menangis ketika mendengar suara keras', 9),
(47, 6, 3,'Berat badan sesuai tingkat usia', 9),
(48, 6, 3,'Tinggi badan sesuai tingkat usia', 9),
(49, 6, 3,'Berat badan sesuai dengan standar tinggi badan', 9),
(50, 6, 3,'Lingkar kepala sesuai tingkat usia', 9),
(51, 6, 3,'Telah diimunisasi sesuai jadwal', 9),
(52, 6, 3,'Menunjuk makanan yang diinginkan', 9),
(53, 6, 3,'Mencari pengasuh atau orangtua', 9),
(54, 9, 3,'Menjerit saat merasa tidak aman ', 9),
(55, 9, 3,'Berat badan sesuai tingkat usia ', 9),
(56, 9, 3,'Tinggi badan sesuai tingkat usia', 9),
(57, 9, 3,'Berat badan sesuai dengan standar tinggi badan', 9),
(58, 9, 3,'Lingkar kepala sesuai tingkat usia', 9),
(59, 9, 3,'Telah diimunisasi sesuai jadwal', 9),
(60, 9, 3,'Menjerit saat merasa tidak aman', 9),
(61, 0, 0,'Mengenali wajah orang terdekat (ibu/ayah)', 10),
(62, 0, 0,'Mengenali suara orang terdekat (ibu/ayah)', 10),
(63, 3, 3,'Memperhatikan benda  yang ada di hadapannya', 10),
(64, 3, 3,'Mendengarkan suara-suara di sekitarnya Ingin tahu lebih dalam dengan benda yang dipegangnya (misal: cara membongkar, membanting, dll)', 10),
(65, 6, 3,'Mengamati berbagai benda yang bergerak', 10),
(66, 9, 3,'Memahami perintah sederhana ', 10),
(67, 0, 0,'Memperhatikan benda bergerak atau suara/mainan yang menggantung  di atas tempat tidur', 11),
(68, 3, 3,'Mengulurkan kedua tangan untuk meminta (misal: digendong, dipangku, dipeluk) ', 11),
(69, 6, 3,'Mengamati benda yang dipegang kemudian dijatuhkan', 11),
(70, 6, 3,'Menjatuhkan benda yang dipegang secara berulang', 11),
(71, 6, 3,'Berpaling ke arah sumber suara', 11),
(72, 9, 3,'Memberi reaksi menoleh  saat namanya dipanggil', 11),
(73, 9, 3,'Mencoba mencari benda yang disembunyikan', 11),
(74, 9, 3,'Mencoba membuka/ menutup gelas/cangkir ', 11),
(75, 0, 0,'Menangis', 4),
(76, 0, 0,'Berteriak', 4),
(77, 0, 0,'Bergumam', 4),
(78, 0, 0,'Berhenti menangis setelah keinginannya terpenuhi (misal: setelah digendong atau diberi susu)', 4),
(79, 3, 3,'Memperhatikan / mendengarkan ucapan orang', 4),
(80, 3, 3,'Meraban atau berceloteh (babbling); seperti ba ba ba)', 4),
(81, 3, 3,'Tertawa kepada orang yang mengajak berkomunikasi', 4),
(82, 6, 3,'Mulai menirukan     kata yang terdiri dari dua suku kata', 4),
(83, 6, 3,'Merespon permainan “cilukba”', 4),
(84, 9, 3,'Menyatakan penolakan dengan menggeleng atau menangis', 4),
(85, 9, 3,'Menunjuk benda yang diinginkan', 4),
(86, 0, 0,'Menatap dan tersenyum', 5),
(87, 0, 0,'Menangis untuk mengekspresi kan ketidak nyamanan (misal, BAK, BAB, lingkungan panas)', 5),
(88, 3, 3,'Merespon dengan gerakan tangan dan kaki', 5),
(89, 3, 3,'Menangis apabila tidak mendapatkan yang diinginkan', 5),
(90, 3, 3,'Merespon dengan menangis/ menggerakkan tubuh pada orang yang belum dikenal', 5),
(91, 6, 3,'Menempelkan kepala bila merasa nyaman dalam pelukan (gendongan) atau meronta kalau merasa tidak nyaman', 5),
(92, 9, 3,'Menyatakan keinginan dengan berbagai gerakan tubuh dan ungkapan kata-kata sederhana', 5),
(93, 9, 3,'Meniru cara menyatakan perasaan (misal, cara memeluk, mencium)', 5),
(94, 0, 0,'Menoleh pada  berbagai suara musik atau bunyibunyian dengan irama teratur ', 13),
(95, 3, 3,'Mendengarkan  berbagai jenis musik atau bunyi-bunyian dengan irama yang teratur', 13),
(96, 3, 3,'Menjatuhkan benda untuk didengar suaranya', 13),
(97, 6, 3,'Melakukan tepuk tangan sederhana dengan irama tertentu', 13),
(98, 3, 3,'Tertarik dengan mainan yang mengeluarkan bunyi', 13),
(99, 9, 3,'Menggerakkan tubuh ketika mendengarkan musik', 13),
(100, 9, 3,'Memainkan alat permainan yang  mengeluarkan bunyi', 13),
(101, 0, 0,'mendengar, menoleh, , atau memperhatikan   musik atau suara  dari pembicaraan orang tua/orang di sekitarnya', 14),
(102, 0, 0,'Melihat obyek yang diatasnya', 14),
(103, 3, 3,'Memperhatikan orang berbicara', 14),
(104, 3, 3,'Memalingkan kepala mengikuti suara orang', 14),
(105, 3, 3,'Memperhatikan jika didengarkan irama lagu dari mainan yang bersuara', 14),
(106, 3, 3,'Mengikuti irama lagu dengan suaranya secara sederhana', 14),
(107, 3, 3,'Mengamati obyek yang berbunyi  di sekitanya', 14),
(108, 6, 3,'Anak tertawa ketika diperlihatkan stimulus yang lucu/aneh', 14),
(109, 6, 3,'Merespon bunyi atau suara dengan gerakan tubuh (misal: bergoyang-goyang) dengan ekspresi wajah yang sesuai', 14),
(110, 9, 3,'Memukul benda dengan irama teratur', 14),
(111, 9, 3,'Bersuara mengikuti irama musik atau lagu', 14),
(112, 0, 0,'Melihat ke gambar atau benda yang ditunjukkan 30 cm dari wajahnya', 15),
(113, 3, 3,'Menoleh atau memalingkan wajah secara spontan ketika ditunjukkan foto/ gambar/cermin dan berusaha menyentuh', 15),
(114, 6, 3,'Berusaha memegang benda, alat tulis yang diletakkan di hadapannya', 15),
(115, 9, 3,'Mencoret di atas media (misal: kertas, tembok)', 15),
(116, 12, 3,'Tertarik pada kegiatan ibadah (meniru gerakan ibadah, meniru bacaan do’a)', 1),
(117, 18, 6,'Menirukan gerakan ibadah dan doa', 1),
(118, 18, 6,'Mulai menunjukkan sikap-sikap baik (seperti yang diajarkan agama) terhadap orang yang sedang beribadah', 1),
(119, 18, 6,'Mengucapkan salam dan  kata-kata baik, seperti maaf, terima kasih pada situasi yang sesuai', 1),
(120, 12, 3,'Berjalan beberapa langkah tanpa bantuan', 7),
(121, 12, 3,'Naik turun tangga atau  tempat  yang lebih tinggi dengan merangkak', 7),
(122, 12, 3,'Dapat bangkit dari posisi duduk', 7),
(123, 12, 3,'Melakukan gerak menendang bola', 7),
(124, 12, 3,'Berguling ke segala arah', 7),
(125, 12, 3,'Berjalan beberapa langkah tanpa bantuan', 7),
(126, 18, 6,'Berjalan sendiri tanpa jatuh', 7),
(127, 18, 6,'Melompat di tempat', 7),
(128, 18, 6,'Naik turun tangga atau tempat yang lebih tinggi dengan bantuan', 7),
(129, 18, 6,'Berjalan mundur beberapa langkah', 7),
(130, 18, 6,'Menarik dan mendorong benda yang ringan (kursi kecil)', 7),
(131, 18, 6,'Melempar bola ke depan tanpa kehilangan keseimbangan', 7),
(132, 18, 6,'Menendang bola ke arah depan', 7),
(133, 18, 6,'Berdiri dengan satu kaki selama satu atau dua detik', 7),
(134, 18, 6,'Berjongkok', 7),
(135, 12, 3,'Membuat coretan bebas', 8),
(136, 12, 3,'Menumpuk  tiga kubus ke atas', 8),
(137, 12, 3,'Memegang gelas dengan dua tangan', 8),
(138, 12, 3,'Memasukkan  benda-benda ke dalam wadah', 8),
(139, 12, 3,'Menumpahkan benda-benda dari wadah', 8),
(140, 18, 6,'Membuat garis vertikal atau horizontal', 8),
(141, 18, 6,'Membalik halaman buku walaupun belum sempurna', 8),
(142, 18, 6,'Menyobek kertas', 8),
(143, 12, 3,'Berat badan sesuai standar usia 2', 9),
(144, 12, 3,'Tinggi badan sesuai standar usia', 9),
(145, 12, 3,'Berat badan sesuai dengan standar tinggi badan', 9),
(146, 12, 3,'Mencuci tangan dengan bantuan', 9),
(147, 12, 3,'Merespon larangan orangtua namun masih memerlukan pengawasan dan bantuan', 9),
(148, 18, 6,'Berat badan sesuai standar usia', 9),
(149, 18, 6,'Tinggi badan sesuai standar usia', 9),
(150, 18, 6,'Berat badan sesuai dengan standar tinggi badan', 9),
(151, 18, 6,'Lingkar kepala sesuai standar pada usia', 9),
(152, 18, 6,'Mencuci tangan sendiri', 9),
(153, 18, 6,'Makan dengan sendok walau belum rapi', 9),
(154, 18, 6,'Menggosok gigi dengan bantuan', 9),
(155, 18, 6,'Memegang tangan orang dewasa ketika di tempat umum', 9),
(156, 18, 6,'Mengenal beberapa penanda rasa sakit (misal: menunjukkan rasa sakit pada bagian badan tertentu) ', 9),
(157, 12, 3,'Menyebut beberapa nama benda, jenis makanan', 16),
(158, 12, 3,'Menanyakan nama benda yang belum dikenal', 16),
(159, 12, 3,'Mengenal beberapa warna dasar (merah, biru, kuning, hijau)', 16),
(160, 12, 3,'Menyebut nama sendiri dan orangorang yang dikenal', 16),
(161, 18, 6,'Mempergunakan alat permainan dengan cara memainkannya tidak beraturan,  seperti balok dipukul-pukul', 16),
(162, 18, 6,'Memahami  gambar wajah orang', 16),
(163, 18, 6,'Memahami milik diri sendiri dan orang lain seperti: milik saya, milik kamu', 16),
(164, 18, 6,'Menyebutkan berbagai nama makanan dan rasanya (misal,garam-asin, gula-manis)', 16),
(167, 12, 3,' Membedakan ukuran benda (besarkecil)', 17),
(168, 12, 3,'Membedakan penampilan yang rapi atau tidak', 17),
(169, 12, 3,'Merangkai puzzle sederhana ', 17),
(170, 18, 6,'Menyusun balok dari besar ke kecil atau sebaliknya', 17),
(171, 18, 6,'Mengetahui akibat dari suatu perlakuannya (misal: menarik taplak meja akan menjatuhkan barang-barang di atasnya)', 17),
(172, 18, 6,'Merangkai puzzle', 17),
(173, 12, 3,'Menyebutkan bilangan tanpa menggunakan jari dari 1 -10 tetapi masih suka ada yang terlewat', 18),
(174, 18, 6,'Menyebutkan angka satu sampai lima dengan menggunakan jari', 18),
(175, 12, 3,'Menunjuk bagian tubuh yang ditanyakan', 19),
(176, 12, 3,'Memahami tema cerita yang didengar', 19),
(177, 18, 6,'Menaruh perhatian pada gambar-gambar dalam buku', 19),
(178, 18, 6,'Memahami  kata-kata sederhana dari ucapan yang didengar', 19),
(179, 12, 3,'Merespons pertanyaan dengan jawaban “Ya atau Tidak” ', 20),
(180, 12, 3,'Mengucapkan kalimat yang terdiri dari dua kata', 20),
(181, 18, 6,'Menjawab pertanyaan dengan  kalimat pendek', 20),
(182, 18, 6,'Menyanyikan lagu sederhana', 20),
(183, 18, 6,'Menyatakan keinginan dengan kalimat  pendek', 20),
(184, 12, 3,'Menunjukkan reaksi marah apabila merasa terganggu, seperti permainannya diambil', 5),
(185, 18, 6,'Menunjukkan reaksi yang berbeda terhadap orang yang baru dikenal', 5),
(186, 18, 6,'Bermain bersama teman tetapi sibuk dengan mainannya sendiri', 5),
(187, 18, 6,'Memperhatikan/mengamati temantemannya yang beraktivitas', 5),
(188, 18, 6,'Mengekspresikan berbagai reaksi emosi  (senang, marah, takut,  kecewa)', 5),
(189, 18, 6,'Menunjukkan reaksi menerima atau menolak kehadiran orang lain', 5),
(190, 18, 6,'Bermain bersama teman dengan mainan yang sama', 5),
(191, 18, 6,'Meniru perilaku orang dewasa yang pernah dilihatnya', 5),
(192, 18, 6,'Makan dan minum sendir.', 5),
(193, 12, 3,'Bisa menyanyikan lagu hanya kata terakhir (misalnya, “burung kakak .....” anak hanya menyebutkan kata “tua”)', 21),
(194, 12, 3,'Merespon berbagai macam suara orang terdekat, musik, atau lagu dengan menggoyangkan badan', 21),
(195, 12, 3,'Mengetahui suara binatang', 21),
(196, 12, 3,'Paham adanya perbedaan suara/bahasa orang di sekitarnya (terutama ibu dan orang terdekatnya', 21),
(197, 18, 6,'Anak mengenali musik dari program audio visual yang disukai (radio, TV, komputer, laptop)', 21),
(198, 18, 6,'Mendengar sesuatu dalam waktu yang lama', 21),
(199, 18, 6,'Secara berulang bermain dengan alat permainan yang mengeluarkan suara', 21),
(200, 18, 6,'Anak tertawa saat mendengar humor yang lucu', 21),
(201, 12, 3,'Menirukan bunyi, suara, atau musik dengan irama yang teratur', 22),
(202, 18, 6,'Bertepuk tangan dan bergerak mengikuti irama dan birama', 22),
(203, 18, 6,'Bergumam lagu dengan 4 bait (misalnya, lagu balonku, bintang kecil, burung kakak tua)', 22),
(204, 18, 6,'Meniru suara binatang', 22),
(205, 18, 6,'Menunjukkan suatu reaksi kalau dilarang atau diperintah', 22),
(206, 12, 3,' Mencoret - coret', 23),
(207, 12, 3,'Mengusap dengan tangan pada kertas/kain dengan menggunakan berbagai media (misal, media bubur aci berwarna, cat air) ', 23),
(208, 18, 6,'Menggambar dari beberapa garis', 23),
(209, 18, 6,'Membentuk suatu karya  sederhana (berbentuk bulat atau lonjong) dari plastisin', 23),
(210, 18, 6,'Menyusun 4-6 balok membentuk suatu model', 23),
(211, 18, 6,'Bertepuk tangan dengan pola sederhana', 23),
(212, 24, 6,'Mulai meniru gerakan berdoa/sembahyang sesuai dengan agamanya', 1),
(213, 24, 6,'Mulai memahami kapan mengucapkan salam, terima kasih, maaf, dsb', 1),
(214, 36, 12,'Mengetahui  perilaku yang berlawanan meskipun belum selalu dilakukan seperti pemahaman perilaku baik-buruk, benar-salah, sopan-tidak sopan', 1),
(215, 36, 12,'Mengetahui  arti kasih dan sayang kepada ciptaan Tuhan', 1),
(216, 36, 12,'Mulai meniru doa pendek sesuai dengan agamanya ', 1),
(217, 24, 6,'Berjalan sambil berjinjit', 7),
(218, 24, 6,'Melompat ke depan dan ke belakang dengan dua kaki', 7),
(219, 24, 6,'Melempar dan menangkap bola', 7),
(220, 24, 6,'Menari mengikuti irama', 7),
(221, 24, 6,'Naik-turun tangga atau tempat yang lebih tinggi/rendah dengan berpegangan', 7),
(222, 36, 12,'Berlari sambil membawa sesuatu yang ringan (bola)', 7),
(223, 36, 12,'Naik-turun tangga atau tempat yang lebih tinggi dengan kaki bergantian', 7),
(224, 36, 12,'Meniti di atas papan yang cukup lebar', 7),
(225, 36, 12,'Melompat turun dari ketinggian kurang lebih 20 cm (di bawah tinggi lutut anak)', 7),
(226, 36, 12,'Meniru gerakan senam sederhana seperti menirukan gerakan pohon, kelinci melompat)', 7),
(227, 36, 12,'Berdiri dengan satu kaki', 7),
(228, 24, 6,'Meremas kertas atau kain dengan menggerakkan lima jari', 8),
(229, 24, 6,'Melipat kain/kertas meskipun belum rapi/lurus', 8),
(230, 24, 6,'Menggunting kertas tanpa pola', 8),
(231, 24, 6,'Koordinasi jari tangan cukup baik untuk memegang benda pipih seperti sikat gigi, sendok', 8),
(232, 36, 12,'Menuang air, pasir, atau biji-bijian ke dalam tempat penampung (mangkuk, ember)', 8),
(234, 36, 12,'Memasukkan benda kecil ke dalam botol (potongan lidi, kerikil, biji-bijian)', 8),
(235, 36, 12,'Meronce benda yang cukup besar', 8),
(236, 36, 12,'Menggunting kertas mengikuti pola garis lurus', 8),
(237, 24, 6,'Berat badan sesuai Tingkat usia', 9),
(238, 24, 6,'Tinggi badan sesuai Tingkat usia', 9),
(239, 24, 6,'Berat badan sesuai dengan standar tinggi badan', 9),
(240, 24, 6,'Lingkar kepala sesuai Tingkat usia', 9),
(241, 24, 6,'Mencuci, membilas, dan mengelap  ketika cuci tangan tanpa bantuan', 9),
(242, 24, 6,'Memberitahu orang dewasa bila sakit', 9),
(243, 24, 6,'Mencuci atau mengganti alat makan bila jatuh', 9),
(244, 36, 12,'Berat badan sesuai Tingkat usia', 9),
(245, 36, 12,'Tinggi badan sesuai Tingkat usia', 9),
(246, 36, 12,'Berat badan sesuai dengan standar tinggi badan', 9),
(247, 36, 12,'Lingkar kepala sesuai Tingkat usia', 9),
(248, 36, 12,'Membersihkan  kotoran  (ingus)', 9),
(249, 36, 12,'Menggosok  gigi', 9),
(250, 36, 12,'Memahami arti warna lampu lalu lintas', 9),
(251, 36, 12,'Mengelap tangan dan muka sendiri', 9),
(252, 36, 12,'Memahami kalau berjalan di sebelah kiri', 9),
(253, 24, 6,'Melihat dan menyentuh benda yang ditunjukkan oleh orang lain', 16),
(254, 24, 6,'Meniru cara pemecahan orang dewasa atau teman', 16),
(255, 24, 6,'Konsentrasi dalam mengerjakan sesuatu tanpa bantuan orangtua', 16),
(256, 24, 6,'Mengeksplorasi sebab dan akibat', 16),
(257, 24, 6,'Mengikuti kebiasaan sehari-hari (mandi, makan, pergi ke sekolah) ', 16),
(258, 36, 12,'Paham apabila ada bagian yang hilang dari suatu pola gambar seperti pada gambar wajah orang matanya tidak ada, mobil bannya copot, dsb', 16),
(259, 36, 12,'Menyebutkan berbagai nama makanan dan rasanya (garam, gula atau cabai)', 16),
(260, 36, 12,'Menyebutkan berbagai macam kegunaan dari benda', 16),
(261, 36, 12,'Memahami persamaan antara dua benda', 16),
(262, 36, 12,'Memahami perbedaan antara dua hal dari jenis yang sama seperti membedakan antara buah rambutan dan pisang; perbedaan antara ayam dan kucing', 16),
(263, 36, 12,'Bereksperimen dengan bahan menggunakan cara baru', 16),
(264, 36, 12,'Mengerjakan tugas sampai selesai', 16),
(265, 36, 12,'Menjawab apa yang akan terjadi selanjutnya dari berbagai kemungkinan', 16),
(266, 36, 12,'Menyebutkan bilangan angka 1-10', 16),
(267, 36, 12,'Mengenal beberapa huruf atau abjad tertentu dari A-z yang pernah dilihatnya', 16),
(268, 24, 6,'Menyebut bagian-bagian suatu gambar seperti gambar wajah orang, mobil, binatang, dsb', 17),
(269, 24, 6,'Mengenal bagian-bagian tubuh (lima bagian)', 17),
(270, 24, 6,'Memahami konsep ukuran (besarkecil, panjang-pendek)', 17),
(271, 24, 6,'Mengenal tiga macam bentuk lingkaran, segitiga, persegi', 17),
(272, 24, 6,'Mulai mengenal pola', 17),
(273, 24, 6,'Memahami simbol angka dan maknanya', 17),
(274, 36, 12,'Menempatkan benda dalam urutan ukuran (paling kecil-paling besar)', 17),
(275, 36, 12,'Mulai mengikuti pola tepuk tangan', 17),
(276, 36, 12,'Mengenal konsep banyak dan sedikit', 17),
(277, 36, 12,'Mengenali alasan mengapa ada sesuatu yang tidak masuk dalam kelompok tertentu', 17),
(278, 36, 12,'Menjelaskan model/karya yang dibuatnya', 17),
(279, 24, 6,'Meniru perilaku orang lain dalam   menggunakan barang', 18),
(280, 24, 6,'Memberikan nama atas karya yang dibuat', 18),
(281, 24, 6,'Melakukan aktivitas seperti kondisi nyata (misal: memegang gagang telpon)', 18),
(282, 36, 12,'Menyebutkan peran dan tugasnya (misal, koki tugasnya memasak)', 18),
(283, 36, 12,'Menggambar atau membentuk sesuatu konstruksi yang mendeskripsikan sesuatu yang spesifik', 18),
(284, 36, 12,'Melakukan aktivitas bersama teman dengan terencana (bermain berkelompok dengan memainkan peran tertentu seperti yang telah direncanakan)', 18),
(285, 24, 6,'Memainkan kata/suara yangdidengar dan diucapkan berulangulang', 19),
(286, 24, 6,'Hafal beberapa lagu anaksederhana', 19),
(287, 24, 6,'Hafal beberapa lagu anaksederhana', 19),
(288, 24, 6,'Memahami perintah sederhanaseperti letakkan mainan di atasmeja, ambil mainan dari dalamkotak', 19),
(289, 36, 12,'Pura-pura membaca cerita bergambar dalambuku dengan kata-kata sendiri', 19),
(290, 36, 12,'Mulai memahami dua perintah yang diberikanbersamaan contoh: ambil mainan di atas mejalalu berikan kepada ibu pengasuh ataupendidik', 19),
(291, 24, 6,'Menggunakan kata tanya dengantepat (apa, siapa, bagaimana,mengapa, dimana)', 20),
(292, 24, 6,'Menggunakan 3 atau 4 kata untukmemenuhi kebutuhannya (misal,mau minum air putih)', 20),
(293, 24, 6,'Mulai menyatakan keinginan denganmengucapkan kalimat sederhana (6 kata)', 20),
(294, 24, 6,'Mulai menceritakan pengalaman yang dialami dengan cerita sederhana', 20),
(295, 24, 6,'Memberi salam setiap mau pergi', 24),
(296, 24, 6,'Memberi rekasi percaya pada orangdewasa', 24),
(297, 24, 6,'Menyatakan perasaan terhadapanak lain', 24),
(298, 24, 6,'Berbagi peran dalam suatupermainan (misal: menjadi dokter,perawat, pasien)', 24),
(299, 36, 12,'Mengikuti aktivitas dalam suatu kegiatanbesar (misal: piknik)', 24),
(300, 36, 12,'Meniru apa yang dilakukan orang dewasa', 24),
(301, 36, 12,'Bereaksi terhadap hal-hal yang tidak benar(marah bila diganggu)', 24),
(302, 36, 12,'Mengatakan perasaan secara verbal', 24),
(303, 24, 6,'Mulai bisa mengungkapkan ketikaingin buang air kecil dan buang airbesar', 25),
(304, 24, 6,'Mulai memahami hak orang lain(harus antri, menunggu giliran.', 25),
(305, 24, 6,'Mulai menunjukkan sikap berbagi,membantu, bekerja bersam.', 25),
(306, 36, 12,'Mulai bisa melakukan buang air kecil tanpa bantuan', 25),
(307, 36, 12,'Bersabar menunggu giliran', 25),
(308, 36, 12,'Mulai menunjukkan sikap toleran sehinggadapat bekerja dalam kelompok', 25),
(309, 36, 12,'Mulai menghargai orang lain', 25),
(310, 36, 12,'Mulai menunjukkan ekspresi menyesal ketikamelakukan kesalahan', 25),
(311, 24, 6,'Bermain secara kooperatif dalamkelompok', 26),
(312, 24, 6,'Peduli dengan orang lain(tersenyum, menanggapi bicara)', 26),
(313, 24, 6,'Membagi pengalaman yang benardan salah pada orang lain', 26),
(314, 24, 6,'Bermain bersama berdasarkanaturan tertentu', 26),
(315, 36, 12,'Membangun kerjasama', 26),
(316, 36, 12,'Memahami adanya perbedaan perasaan(teman takut, saya tidak)', 26),
(317, 36, 12,'Meminjam dan meminjamkan mainan', 26),
(318, 24, 6,'Memperhatikan dan mengenali suarayang bernyanyi atau berbicara', 21),
(319, 24, 6,'Mengenali berbagai macam suara darikendaraan', 21),
(320, 24, 6,'Meminta untuk diperdengarkan lagu favorit secara berulang', 21),
(321, 24, 6,'. Menyanyi sampai tuntasdengan irama yang benar(nyanyian pendek atau 4 bait)', 27),
(322, 24, 6,'Menyanyikan lebih dari 3 lagudengan irama yang yang benarsampai tuntas (nyanyian pendekatau 4 bait)', 27),
(323, 24, 6,'Bersama teman-temanmenyanyikan lagu', 27),
(324, 24, 6,'Bernyanyi mengikuti irama denganbertepuk tangan ataumenghentakkan kaki', 27),
(325, 24, 6,'Meniru gerakan berbagai binatang', 27),
(326, 24, 6,'Paham bila orang terdekatnya (ibu)menegur', 27),
(327, 24, 6,'Mencontoh gerakan orang lain', 27),
(328, 24, 6,'Bertepuk tangan sesuai irama', 27),
(329, 36, 12,'Mendengarkan atau menyanyikan lagu', 27),
(330, 36, 12,'Menggerakkan tubuh sesuai irama', 27),
(331, 36, 12,'Bertepuk tangan sesuai irama musik', 27),
(332, 36, 12,'Meniru aktivitas orang baik secara langsungmaupun melalui media. (misal, caraminum/cara bicara/perilaku seperti ibu)', 27),
(333, 36, 12,'Bertepuk tangan dengan pola yang berirama(misalnya bertepuk tangan sambil mengikutiirama nyanyian)', 27),
(334, 24, 6,'Menggambar benda-benda lebihspesifik', 28),
(335, 24, 6,'Mengamati dan membedakan bendadi sekitarnya yang di dalam rumah', 27),
(336, 36, 12,'Menggambar dengan menggunakan beragammedia (cat air, spidol, alat menggambar) dancara (seperti finger painting, cat air, dll)', 28),
(337, 36, 12,'Membentuk sesuatu dengan plastisin', 28),
(338, 36, 12,'Mengamati dan membedakan benda disekitarnya yang di luar rumah', 28),
(339, 48, 12,'Mengetahui agama yang dianutnya', 1),
(340, 48, 12,'Meniru gerakan beribadah denganurutan yang benar', 1),
(341, 48, 12,'Mengucapkan doa sebelum dan/atausesudah melakukan sesuatu', 1),
(342, 48, 12,'Mengenal perilaku baik/sopan danburuk', 1),
(343, 48, 12,'Membiasakan diri berperilaku baik', 1),
(344, 48, 12,'Mengucapkan salam dan membalassalam', 1),
(345, 60, 12,'Mengenal agama yang dianut', 1),
(346, 60, 12,'Mengerjakan ibadah', 1),
(347, 60, 12,'Berperilaku jujur, penolong, sopan, hormat,sportif, dsb', 1),
(348, 60, 12,'Menjaga kebersihan diri dan lingkungan', 1),
(349, 60, 12,'Mengetahui hari besar agama', 1),
(350, 60, 12,'Menghormati (toleransi) agama orang lain', 1),
(351, 48, 12,'Menirukan gerakan binatang, pohontertiup angin, pesawat terbang, dsb', 7),
(352, 48, 12,'Melakukan gerakan menggantung(bergelayut)', 7),
(353, 48, 12,'Melakukan gerakan melompat,meloncat, dan berlari secaraterkoordinasi', 7),
(354, 48, 12,'Melempar sesuatu secara terarah', 7),
(355, 48, 12,'Menangkap sesuatu secara tepat', 7),
(356, 48, 12,'Melakukan gerakan antisipasi', 7),
(357, 48, 12,'Menendang sesuatu secara terarah', 7),
(358, 48, 12,'Memanfaatkan alat permainan di luarkelas', 7),
(359, 60, 12,'Melakukan gerakan tubuh secaraterkoordinasi untuk melatih kelenturan,keseimbangan, dan kelincahan', 7),
(360, 60, 12,'Melakukan koordinasi gerakan mata-kakitangan-kepala dalam menirukan tarian atausenam', 7),
(361, 60, 12,'Melakukan permainan fisik dengan aturan', 7),
(362, 60, 12,'Terampil menggunakan tangan kanan dankiri', 7),
(363, 60, 12,'Melakukan kegiatan kebersihan diri', 7),
(364, 48, 12,'Membuat garis vertikal, horizontal,lengkung kiri/kanan, miringkiri/kanan, dan lingkaran', 8),
(365, 48, 12,'Menjiplak bentuk', 8),
(366, 48, 12,'Mengkoordinasikan mata dan tanganuntuk melakukan gerakan yang rumit', 8),
(367, 48, 12,'Melakukan gerakan manipulatifuntuk menghasilkan suatu bentukdengan menggunakan berbagai media', 8),
(368, 48, 12,'Mengekspresikan diri denganberkarya seni menggunakan berbagaimedia', 8),
(369, 48, 12,'Mengontrol gerakan tangan yangmeggunakan otot halus (menjumput,mengelus, mencolek, mengepal,memelintir, memilin, memeras)', 8),
(370, 60, 12,'Menggambar sesuai gagasannya', 8),
(371, 60, 12,'Meniru bentuk', 8),
(372, 60, 12,'Melakukan eksplorasi dengan berbagaimedia dan kegiatan', 8),
(373, 60, 12,'Menggunakan alat tulis dan alat makandengan benar', 8),
(374, 60, 12,'Menggunting sesuai dengan pola', 8),
(375, 60, 12,'Menempel gambar dengan tepat', 8),
(376, 60, 12,'Mengekspresikan diri melalui gerakanmenggambar secara rinci', 8),
(378, 48, 12,'Berat badan sesuai tingkat usia', 9),
(379, 48, 12,'Tinggi badan sesuai tingkat usia', 9),
(380, 48, 12,'Berat badan sesuai dengan standartinggi badan', 9),
(381, 48, 12,'Lingkar kepala sesuai tingkat usia', 9),
(382, 48, 12,'Menggunakan toilet (penggunaan air,membersihkan diri) dengan bantuanminimal', 9),
(383, 48, 12,'Memahami berbagai alarm bahaya(kebakaran, banjir, gempa)', 9),
(384, 48, 12,'Mengenal rambu lalu lintas yang adadi jalan', 9),
(385, 60, 12,'Berat badan sesuai tingkat usia', 9),
(386, 60, 12,'Tinggi badan sesuai standar usia', 9),
(387, 60, 12,'Berat badan sesuai dengan standar tinggibadan', 9),
(388, 60, 12,'Lingkar kepala sesuai tingkat usia', 9),
(389, 60, 12,'Menutup hidung dan mulut (misal, ketikabatuk dan bersin)', 9),
(390, 60, 12,'Membersihkan, dan membereskan tempatbermain', 9),
(391, 60, 12,'Mengetahui situasi yang membahayakandiri', 9),
(392, 60, 12,'Memahami tata cara menyebrang', 9),
(393, 60, 12,'Mengenal kebiasaan buruk bagi kesehatan(rokok, minuman keras)', 9),
(394, 48, 12,'Mengenal benda berdasarkan fungsi(pisau untuk memotong, pensil untukmenulis)', 16),
(395, 48, 12,'Menggunakan benda-benda sebagaipermain', 16),
(396, 48, 12,'Mengenal konsep sederhana dalamkehidupan sehari-hari (gerimis, hujan,gelap, terang, temaram, dsb)', 16),
(397, 48, 12,'Mengetahui konsep banyak dansedikit', 16),
(398, 48, 12,'Mengkreasikan sesuatu sesuai denganidenya sendiri yang terkait denganberbagai pemecahan masalah', 16),
(399, 48, 12,'Mengamati benda dan gejala denganrasa ingin tahu', 16),
(400, 48, 12,'Mengenal pola kegiatan danmenyadari pentingnya waktu', 16),
(401, 48, 12,'Memahami posisi/kedudukan dalamkeluarga, ruang, lingkungan sosial(misal: sebagai pesertadidik/anak/teman)', 16),
(402, 60, 12,'Menunjukkan aktivitas yang bersifateksploratif dan menyelidik (seperti: apa yangterjadi ketika air ditumpahkan)', 16),
(403, 60, 12,'Memecahkan masalah sederhana dalamkehidupan sehari-hari dengan cara yangfleksibel dan diterima sosial', 16),
(404, 60, 12,'Menerapkan pengetahuan atau pengalamandalam konteks yang baru', 16),
(405, 60, 12,'Menunjukkan sikap kreatif dalammenyelesaikan masalah (ide, gagasan diluar kebiasaan)', 16),
(406, 48, 12,'. Mengklasifikasikan bendaberdasarkan fungsi, bentuk atauwarna atau ukuran', 17),
(407, 48, 12,'Mengenal gejala sebab-akibat yangterkait dengan dirinya', 17),
(408, 48, 12,'Mengklasifikasikan benda ke dalamkelompok yang sama atau kelompokyang sejenis atau kelompok yangberpasangan dengan 2 variasi', 17),
(409, 48, 12,'Mengenal pola (misal, AB-AB danABC-ABC) dan mengulanginya', 17),
(410, 48, 12,'Mengurutkan benda berdasarkan 5seriasi ukuran atau warna', 17),
(411, 48, 12,'Mengenal perbedaan berdasarkan ukuran:“lebih dari”; “kurang dari”; dan “paling/ter”', 17),
(412, 48, 12,'Menunjukkan inisiatif dalam memilih temapermainan (seperti: ”ayo kita bermainpura-pura seperti burung”)', 17),
(413, 48, 12,'Menyusun perencanaan kegiatan yang akandilakukan', 17),
(414, 60, 12,'Mengenal sebab-akibat tentanglingkungannya (angin bertiupmenyebabkandaun bergerak, air dapat menyebabkansesuatu menjadi basah)', 17),
(415, 60, 12,'Mengklasifikasikan benda berdasarkanwarna, bentuk, dan ukuran (3 variasi)', 17),
(416, 60, 12,'Mengklasifikasikan benda yang lebihbanyak ke dalam kelompok yang sama ataukelompok yang sejenis, atau kelompokberpasangan yang lebih dari 2 variasi', 17),
(417, 60, 12,'Mengenal pola ABCD-ABCD', 17),
(418, 60, 12,'Mengurutkan benda berdasarkan ukurandari paling kecil ke paling besar atausebaliknya', 17),
(419, 48, 12,'Membilang banyak benda satu sampaisepuluh', 18),
(420, 48, 12,'Mengenal konsep bilangan', 18),
(421, 48, 12,'Mengenal lambang bilangan', 18),
(422, 48, 12,'Mengenal lambang huruf', 18),
(423, 60, 12,'Menyebutkan lambang bilangan 1-10', 18),
(424, 60, 12,'Menggunakan lambang bilangan untukmenghitung', 18),
(425, 60, 12,'Mencocokkan bilangan dengan lambangbilangan', 18),
(426, 60, 12,'Mengenal berbagai macam lambang hurufvokal dan konsonan', 18),
(427, 60, 12,'Merepresentasikan berbagai macam bendadalam bentuk gambar atau tulisan (adabenda pensil yang diikuti tulisan dangambar pensil)', 18),
(428, 48, 12,'Menyimak perkataan orang lain(bahasa ibu atau bahasa lainnya)', 19),
(429, 48, 12,'Mengerti dua perintah yang diberikanbersamaan', 19),
(430, 48, 12,'Memahami cerita yang dibacakan', 19),
(431, 48, 12,'Mengenal perbendaharaan katamengenai kata sifat (nakal, pelit, baikhati, berani, baik, jelek, dsb)', 19),
(432, 48, 12,'Mendengar dan membedakan bunyibunyian dalam Bahasa Indonesia(contoh, bunyi dan ucapan harussama)', 19),
(433, 60, 12,'Mengerti beberapa perintah secarabersamaan', 19),
(434, 60, 12,'Mengulang kalimat yang lebih kompleks', 19),
(435, 60, 12,'Memahami aturan dalam suatu permainan', 19),
(436, 60, 12,'Senang dan menghargai bacaan', 19),
(437, 48, 12,'Mengulang kalimat sederhana', 20),
(438, 48, 12,'Bertanya dengan kalimat yang bena', 20),
(439, 48, 12,'Menjawab pertanyaan sesuaipertanyaan', 20),
(440, 48, 12,'Mengungkapkan perasaan dengankata sifat (baik, senang, nakal, pelit,baik hati, berani, baik, jelek, dsb)', 20),
(441, 48, 12,'Menyebutkan kata-kata yang dikenal', 20),
(442, 48, 12,'Mengutarakan pendapat kepadaorang lain', 20),
(443, 48, 12,'Menyatakan alasan terhadapsesuatu yang diinginkan atauketidaksetujuan', 20),
(444, 48, 12,'Menceritakan kembalicerita/dongeng yang pernahdidengar', 20),
(445, 48, 12,'Memperkaya perbendaharaan kata', 20),
(446, 48, 12,'Berpartisipasi dalam percakapan', 20),
(447, 60, 12,'Menjawab pertanyaan yang lebih kompleks', 20),
(448, 60, 12,'Menyebutkan kelompok gambar yangmemiliki bunyi yang sama', 20),
(449, 60, 12,'Berkomunikasi secara lisan, memilikiperbendaharaan kata, serta mengenalsimbol-simbol untuk persiapan membaca,menulis dan berhitung', 20),
(450, 60, 12,'Menyusun kalimat sederhana dalamstruktur lengkap (pokok kalimat-predikatketerangan)', 20),
(451, 60, 12,'Memiliki lebih banyak kata-kata untukmengekpresikan ide pada orang lain', 20),
(452, 60, 12,'Melanjutkan sebagian cerita/dongeng yangtelah diperdengarkan', 20),
(453, 60, 12,'Menunjukkkan pemahaman konsep-konsepdalam buku cerita', 20),
(454, 48, 12,'Mengenal simbol-simbol', 29),
(456, 48, 12,'Mengenal suara–suara hewan/bendayang ada di sekitarnya', 29),
(457, 48, 12,'Membuat coretan yang bermakna', 29),
(458, 48, 12,'Meniru (menuliskan danmengucapkan) huruf A-Z', 29),
(459, 60, 12,'Menyebutkan simbol-simbol huruf yangdikenal', 29),
(460, 60, 12,'Mengenal suara huruf awal dari namabenda-benda yang ada di sekitarnya', 29),
(461, 60, 12,'Menyebutkan kelompok gambar yangmemiliki bunyi/huruf awal yang sama.', 29),
(462, 60, 12,'Memahami hubungan antara bunyi danbentuk huruf', 29),
(463, 60, 12,'Membaca nama sendiri', 29),
(464, 60, 12,'Menuliskan nama sendiri', 29),
(466, 60, 12,'Memahami arti kata dalam cerita', 29),
(467, 48, 12,'Menunjukkan sikap mandiri dalammemilih kegiatan', 24),
(468, 48, 12,'Mengendalikan perasaan', 24),
(469, 48, 12,'Menunjukkan rasa percaya diri', 24),
(470, 48, 12,'Memahami peraturan dan disiplin', 24),
(471, 48, 12,'Memiliki sikap gigih (tidak mudahmenyerah)', 24),
(472, 48, 12,'Bangga terhadap hasil karya sendiri', 24),
(473, 60, 12,'Memperlihatkan kemampuan diri untukmenyesuaikan dengan situasi', 24),
(474, 60, 12,'Memperlihatkan kehati-hatian kepada orangyang belum dikenal (menumbuhkankepercayaan pada orang dewasa yang tepat)', 24),
(475, 60, 12,'Mengenal perasaan sendiri danmengelolanya secara wajar (mengendalikandiri secara wajar)', 24),
(476, 48, 12,'Menjaga diri sendiri darilingkungannya', 25),
(477, 48, 12,'Menghargai keunggulan orang lain', 25),
(478, 48, 12,'Mau berbagi, menolong, danmembantu teman', 25),
(479, 60, 12,'Tahu akan hak nya', 25),
(480, 60, 12,'Mentaati aturan kelas (kegiatan, aturan)', 25),
(481, 60, 12,'Mengatur diri sendiri', 25),
(482, 60, 12,'Bertanggung jawab atas perilakunya untukkebaikan diri sendiri', 25),
(483, 48, 12,'Menunjukan antusiasme dalammelakukan permainan kompetitifsecara positif', 26),
(484, 48, 12,'Menaati aturan yang berlaku dalamsuatu permainan', 26),
(485, 48, 12,'Menghargai orang lain', 26),
(486, 48, 12,'Menunjukkan rasa empati', 26),
(487, 60, 12,'Bermain dengan teman sebaya', 26),
(488, 60, 12,'Mengetahui perasaan temannya danmerespon secara wajar', 26),
(489, 60, 12,'Berbagi dengan orang lain', 26),
(490, 60, 12,'Menghargai hak/pendapat/karya orang lain', 26),
(491, 60, 12,'Menggunakan cara yang diterima secarasosial dalam menyelesaikan masalah(menggunakan fikiran untuk menyelesaikanmasalah)', 26),
(492, 60, 12,'Bersikap kooperatif dengan teman', 26),
(493, 60, 12,'Menunjukkan sikap toleran', 26),
(494, 60, 12,'Mengekspresikan emosi yang sesuai dengankondisi yang ada (senang-sedih-antusiasdsb)', 26),
(495, 60, 12,'Mengenal tata krama dan sopan santunsesuai dengan nilai sosial budaya setempat', 26),
(496, 48, 12,'Senang mendengarkan berbagaimacam musik atau lagu kesukaannya', 30),
(497, 48, 12,'Memainkan alat musik/instrumen/benda yang dapat membentuk irama yang teratur', 30),
(498, 60, 12,'Anak bersenandung atau bernyanyi sambilmengerjakan sesuatu', 30),
(499, 60, 12,'Memainkan alat musik/instrumen/bendabersama teman', 30),
(500, 48, 12,'Memilih jenis lagu yang disukai', 28),
(501, 48, 12,'Bernyanyi sendiri', 28),
(502, 48, 12,'Menggunakan imajinasi untukmencerminkan perasaan dalamsebuah peran', 28),
(503, 48, 12,'Membedakan peran fantasi dankenyataan', 28),
(504, 48, 12,'Menggunakan dialog, perilaku, danberbagai materi dalam menceritakansuatu cerita', 28),
(505, 48, 12,'Mengekspresikan gerakan denganirama yang bervariasi', 28),
(506, 48, 12,'Menggambar objek di sekitarnya', 28),
(507, 48, 12,'Membentuk berdasarkan objek yangdilihatnya (mis. dengan plastisin,tanah liat)', 28),
(508, 48, 12,'Mendeskripsikan sesuatu (sepertibinatang) dengan ekspresif yangberirama (contoh, anak menceritakangajah dengan gerak dan mimiktertentu)', 28),
(509, 48, 12,'Mengkombinasikan berbagai warnaketika menggambar atau mewarnai', 28),
(510, 60, 12,'Menyanyikan lagu dengan sikap yang benar', 28),
(511, 60, 12,'Menggunakan berbagai macam alat musiktradisional maupun alat musik lain untukmenirukan suatu irama atau lagu tertentu', 28),
(512, 60, 12,'Bermain drama sederhana', 28),
(513, 60, 12,'Menggambar berbagai macam bentuk yangberagam', 28),
(514, 60, 12,'Melukis dengan berbagai cara dan objek', 28),
(515, 60, 12,'Membuat karya seperti bentuksesungguhnya dengan berbagai bahan(kertas, plastisin, balok, dll)', 28);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indexes for table `lingkup_perkembangan`
--
ALTER TABLE `lingkup_perkembangan`
  ADD PRIMARY KEY (`id_lingkup`);

--
-- Indexes for table `murid`
--
ALTER TABLE `murid`
  ADD PRIMARY KEY (`id_murid`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indexes for table `orangtua`
--
ALTER TABLE `orangtua`
  ADD PRIMARY KEY (`id_ortu`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id_report`),
  ADD KEY `report_ibfk_1` (`id_guru`),
  ADD KEY `report_ibfk_2` (`id_murid`),
  ADD KEY `report_ibfk_3` (`id_kegiatan`),
  ADD KEY `id_tppa` (`id_tppa`);

--
-- Indexes for table `tppa`
--
ALTER TABLE `tppa`
  ADD PRIMARY KEY (`id_tppa`),
  ADD KEY `tppa_ibfk_1` (`id_lingkup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lingkup_perkembangan`
--
ALTER TABLE `lingkup_perkembangan`
  MODIFY `id_lingkup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `murid`
--
ALTER TABLE `murid`
  MODIFY `id_murid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `orangtua`
--
ALTER TABLE `orangtua`
  MODIFY `id_ortu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id_report` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tppa`
--
ALTER TABLE `tppa`
  MODIFY `id_tppa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=516;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `report_ibfk_1` FOREIGN KEY (`id_guru`) REFERENCES `guru` (`id_guru`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `report_ibfk_2` FOREIGN KEY (`id_murid`) REFERENCES `murid` (`id_murid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `report_ibfk_3` FOREIGN KEY (`id_kegiatan`) REFERENCES `kegiatan` (`id_kegiatan`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `report_ibfk_4` FOREIGN KEY (`id_tppa`) REFERENCES `tppa` (`id_tppa`);

--
-- Constraints for table `tppa`
--
ALTER TABLE `tppa`
  ADD CONSTRAINT `tppa_ibfk_1` FOREIGN KEY (`id_lingkup`) REFERENCES `lingkup_perkembangan` (`id_lingkup`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
