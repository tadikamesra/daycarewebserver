<?php
    class Report{
        private $conn;
        private $table_name = "report";

        //object properties 
        public $id_report;
        public $tanggal;
        public $waktu;
        public $id_murid;
        public $id_guru;
        public $id_kegiatan;
        public $id_tppa;
        public $keterangan;

        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        public function add()
        {
            $query = "INSERT INTO ".$this->table_name." SET 
                        tanggal=:tanggal,
                        waktu=:waktu,
                        id_murid=:id_murid,
                        id_guru=:id_guru,
                        id_kegiatan=:id_kegiatan,
                        keterangan=:keterangan";
            
            $stmt = $this->conn->prepare($query);

            $this->tanggal=htmlspecialchars(strip_tags($this->tanggal));
            $this->waktu=htmlspecialchars(strip_tags($this->waktu));
            $this->id_murid=htmlspecialchars(strip_tags($this->id_murid));
            $this->id_guru=htmlspecialchars(strip_tags($this->id_guru));
            $this->id_kegiatan=htmlspecialchars(strip_tags($this->id_kegiatan));
            $this->keterangan=htmlspecialchars(strip_tags($this->keterangan));

            $stmt->bindParam(":tanggal", $this->tanggal);
            $stmt->bindParam(":waktu", $this->waktu);
            $stmt->bindParam(":id_murid", $this->id_murid);
            $stmt->bindParam(":id_guru", $this->id_guru);
            $stmt->bindParam(":id_kegiatan", $this->id_kegiatan);
            $stmt->bindParam(":keterangan", $this->keterangan);

            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
        }

        public function addTPPA()
        {
            $query = "INSERT INTO ".$this->table_name." SET 
                        tanggal=:tanggal,
                        waktu=:waktu,
                        id_murid=:id_murid,
                        id_guru=:id_guru,
                        id_kegiatan=:id_kegiatan,
                        id_tppa=:id_tppa,
                        keterangan=:keterangan";
            
            $stmt = $this->conn->prepare($query);

            $this->tanggal=htmlspecialchars(strip_tags($this->tanggal));
            $this->waktu=htmlspecialchars(strip_tags($this->waktu));
            $this->id_murid=htmlspecialchars(strip_tags($this->id_murid));
            $this->id_guru=htmlspecialchars(strip_tags($this->id_guru));
            $this->id_kegiatan=htmlspecialchars(strip_tags($this->id_kegiatan));
            $this->id_tppa=htmlspecialchars(strip_tags($this->id_tppa));
            $this->keterangan=htmlspecialchars(strip_tags($this->keterangan));

            $stmt->bindParam(":tanggal", $this->tanggal);
            $stmt->bindParam(":waktu", $this->waktu);
            $stmt->bindParam(":id_murid", $this->id_murid);
            $stmt->bindParam(":id_guru", $this->id_guru);
            $stmt->bindParam(":id_kegiatan", $this->id_kegiatan);
            $stmt->bindParam(":id_tppa", $this->id_tppa);
            $stmt->bindParam(":keterangan", $this->keterangan);

            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
        }

        public function update()
        {
            $query = "UPDATE ".$this->table_name." SET 
                        waktu=:waktu,
                        id_murid=:id_murid,
                        id_guru=:id_guru,
                        id_kegiatan=:id_kegiatan,
                        keterangan=:keterangan
                        WHERE id_report=:id_report";
            
            $stmt = $this->conn->prepare($query);

            $this->id_report=htmlspecialchars(strip_tags($this->id_report));
            $this->waktu=htmlspecialchars(strip_tags($this->waktu));
            $this->id_murid=htmlspecialchars(strip_tags($this->id_murid));
            $this->id_guru=htmlspecialchars(strip_tags($this->id_guru));
            $this->id_kegiatan=htmlspecialchars(strip_tags($this->id_kegiatan));
            $this->keterangan=htmlspecialchars(strip_tags($this->keterangan));

            $stmt->bindParam(":id_report", $this->id_report);
            $stmt->bindParam(":waktu", $this->waktu);
            $stmt->bindParam(":id_murid", $this->id_murid);
            $stmt->bindParam(":id_guru", $this->id_guru);
            $stmt->bindParam(":id_kegiatan", $this->id_kegiatan);
            $stmt->bindParam(":keterangan", $this->keterangan);

            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
        }
        
        public function updateTPPA(){
            $query = "UPDATE ".$this->table_name." SET 
                        waktu=:waktu,
                        id_murid=:id_murid,
                        id_guru=:id_guru,
                        id_kegiatan=:id_kegiatan,
                        id_tppa=:id_tppa,
                        keterangan=:keterangan
                        WHERE id_report=:id_report";
            
            $stmt = $this->conn->prepare($query);

            $this->id_report=htmlspecialchars(strip_tags($this->id_report));
            $this->waktu=htmlspecialchars(strip_tags($this->waktu));
            $this->id_murid=htmlspecialchars(strip_tags($this->id_murid));
            $this->id_guru=htmlspecialchars(strip_tags($this->id_guru));
            $this->id_kegiatan=htmlspecialchars(strip_tags($this->id_kegiatan));
            $this->id_tppa=htmlspecialchars(strip_tags($this->id_tppa));
            $this->keterangan=htmlspecialchars(strip_tags($this->keterangan));

            $stmt->bindParam(":id_report", $this->id_report);
            $stmt->bindParam(":waktu", $this->waktu);
            $stmt->bindParam(":id_murid", $this->id_murid);
            $stmt->bindParam(":id_guru", $this->id_guru);
            $stmt->bindParam(":id_kegiatan", $this->id_kegiatan);
            $stmt->bindParam(":id_tppa", $this->id_tppa);
            $stmt->bindParam(":keterangan", $this->keterangan);

            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
        }
        
        public function read()
        {
            $query = "SELECT * FROM ".$this->table_name;

            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        public function readByMurid()
        {
            $query = "SELECT * FROM ".$this->table_name." WHERE id_murid=:id_murid";

            $stmt = $this->conn->prepare($query);
            
            //sanitize
            $this->id_murid=htmlspecialchars(strip_tags($this->id_murid));

            //bind given value
            $stmt->bindparam(":id_murid", $this->id_murid);

            $stmt->execute();

            return $stmt;
        }
    }
?>