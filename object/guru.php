<?php
    class Guru{
        private $conn;
        private $table_name = "guru";

        //object properties 
        public $id_guru;
        public $nama;
        public $nip;
        public $email;
        public $jk;
        public $telp;
        public $password;
        public $previlege;

        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        public function add()
        {
            $query = "INSERT INTO ".$this->table_name." SET 
                        nama=:nama,
                        nip=:nip,
                        email=:email,
                        jk=:jk,
                        telp=:telp,
                        password=:password";
            
            $stmt = $this->conn->prepare($query);

            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->nip=htmlspecialchars(strip_tags($this->nip));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->jk=htmlspecialchars(strip_tags($this->jk));
            $this->telp=htmlspecialchars(strip_tags($this->telp));
            $this->password=htmlspecialchars(strip_tags($this->password));

            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":nip", $this->nip);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":jk", $this->jk);
            $stmt->bindParam(":telp", $this->telp);
            $stmt->bindParam(":password", $this->password);

            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
        }

        public function list()
        {
            //select all query
            $query = "SELECT * FROM guru
                        WHERE id_guru";

            //prepare query 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        public function read()
        {
            $query = "SELECT * FROM ".$this->table_name. "WHERE id_guru=:id_guru";
            $stmt = $this->conn->prepare($query);

            $this->id_guru=htmlspecialchars(strip_tags($this->id_guru));

            $stmt->bindparam(":id_guru", $this->id_guru);

            $stmt->execute();

            return $stmt;
        }
        
        public function update()
        {
            $query = "UPDATE ".$this->table_name." SET 
                        nama=:nama,
                        nip=:nip,
                        email=:email,
                        jk=:jk,
                        telp=:telp,
                        last_update=now()
                        WHERE id_guru=:id_guru";
            
            $stmt = $this->conn->prepare($query);

            $this->id_guru=htmlspecialchars(strip_tags($this->id_guru));
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->nip=htmlspecialchars(strip_tags($this->nip));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->jk=htmlspecialchars(strip_tags($this->jk));
            $this->telp=htmlspecialchars(strip_tags($this->telp));
            //$this->password=htmlspecialchars(strip_tags($this->password));
            

            $stmt->bindParam(":id_guru", $this->id_guru);
            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":nip", $this->nip);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":jk", $this->jk);
            $stmt->bindParam(":telp", $this->telp);
            //$stmt->bindParam(":password", $this->password);

            // execute query
            if($stmt->execute()){
                return true;
            }
            return false;
        }

        public function delete()
        {
            $query = "DELETE FROM ".$this->table_name." WHERE id_guru=:id";

            $stmt = $this->conn->prepare($query);

            $this->id_guru=htmlspecialchars(strip_tags($this->id_guru));
            
            $stmt->bindParam(":id", $this->id_guru);
            
            // execute query
            if($stmt->execute()){
                return true;
            }
            return false;
        }
        
        //login user
        public function login()
        {
            $query = "SELECT * FROM ". $this->table_name ." 
                    WHERE email=:email AND password=:password";
            
            //prepare query 
            $stmt = $this->conn->prepare($query);

            // sanitize
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->password=htmlspecialchars(strip_tags($this->password));
        
            // bind given email value
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":password", $this->password);

            $stmt->execute();

            return $stmt;
        }

        // check if given email exist in the database
        function emailExists(){        
            // query to check if email exists
            $query = "SELECT * FROM " . $this->table_name . "
                    WHERE email=:email LIMIT 0,1";
        
            // prepare the query
            $stmt = $this->conn->prepare( $query );
        
            // sanitize
            $this->email=htmlspecialchars(strip_tags($this->email));
        
            // bind given email value
            $stmt->bindParam(':email', $this->email);
        
            // execute the query
            $stmt->execute();
        
            // get number of rows
            $num = $stmt->rowCount();
        
            // if email exists, assign values to object properties for easy access and use for php sessions
            if($num>0){
                // return true because email exists in the database
                return true;
            }        
            // return false if email does not exist in the database
            return false;
        }

    }
?>