<?php
    class Murid{
        private $conn;
        private $table_name = "murid";

        //object properties 
        public $id_murid;
        public $nik;
        public $nama;
        public $tgl_lahir;
        public $jk;
        public $bb;
        public $tb;
        public $id_ortu;

        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        public function add()
        {
            $query = "INSERT INTO ".$this->table_name." SET 
                        nama=:nama,
                        nik=:nik,
                        tgl_lahir=:tgl_lahir,
                        jk=:jk,
                        bb=:bb,
                        tb=:tb,
                        id_ortu=:ortu";
            
            $stmt = $this->conn->prepare($query);

            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->nik=htmlspecialchars(strip_tags($this->nik));
            $this->tgl_lahir=htmlspecialchars(strip_tags($this->tgl_lahir));
            $this->jk=htmlspecialchars(strip_tags($this->jk));
            $this->bb=htmlspecialchars(strip_tags($this->bb));
            $this->tb=htmlspecialchars(strip_tags($this->tb));
            $this->id_ortu=htmlspecialchars(strip_tags($this->id_ortu));

            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":nik", $this->nik);
            $stmt->bindParam(":tgl_lahir", $this->tgl_lahir);
            $stmt->bindParam(":jk", $this->jk);
            $stmt->bindParam(":bb", $this->bb);
            $stmt->bindParam(":tb", $this->tb);
            $stmt->bindParam(":ortu", $this->id_ortu);

            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
        }

        public function list()
        {
            //select all query
            $query = "SELECT * FROM murid
                        WHERE id_murid";

            //prepare query 
            $stmt = $this->conn->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        public function read()
        {
            $query = "SELECT * FROM ".$this->table_name. "WHERE nik=:nik";
            $stmt = $this->conn->prepare($query);

            $this->nik=htmlspecialchars(strip_tags($this->nik));

            $stmt->bindparam(":nik", $this->nik);

            $stmt->execute();

            return $stmt;
        }

        public function update()
        {
            $query = "UPDATE ".$this->table_name ." SET 
            nik=:nik,
            nama=:nama, 
            tgl_lahir=:tgl_lahir, 
            jk=:jk,
            bb=:bb,
            tb=:tb,
            id_ortu=:ortu,
            last_update=now()
            WHERE id_murid=:id";

            // prepare query
            $stmt = $this->conn->prepare($query);
            
            // sanitize
            $this->id_murid=htmlspecialchars(strip_tags($this->id_murid));
            $this->nik=htmlspecialchars(strip_tags($this->nik));
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->tgl_lahir=htmlspecialchars(strip_tags($this->tgl_lahir));
            $this->jk=htmlspecialchars(strip_tags($this->jk));
            $this->bb=htmlspecialchars(strip_tags($this->bb));
            $this->tb=htmlspecialchars(strip_tags($this->tb));
            $this->id_ortu=htmlspecialchars(strip_tags($this->id_ortu));

            // bind values
            $stmt->bindParam(":id", $this->id_murid);
            $stmt->bindParam(":nik", $this->nik);
            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":tgl_lahir", $this->tgl_lahir);
            $stmt->bindParam(":jk", $this->jk);
            $stmt->bindParam(":bb", $this->bb);
            $stmt->bindParam(":tb", $this->tb);
            $stmt->bindParam(":ortu", $this->id_ortu);

            if($stmt->execute())
            {
                return true;
            }
            return false; 
        }
    }
?>