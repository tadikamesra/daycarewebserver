<?php
class TPPA{
    private $conn;
    private $table_name = "tppa";

    //object properties 
    public $id_tppa;
    public $usia;
    public $interval;
    public $keterangan;
    public $id_lingkup;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    public function read()
    {
        $query = "SELECT * FROM ".$this->table_name;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function getTppa($lingkup, $usia)
    {
        $query = "SELECT * FROM `".$this->table_name."` WHERE `id_lingkup`='$lingkup' AND `usia`<='$usia' AND (`usia`+`interval`)>='$usia'";
        $stmt = $this->conn->prepare($query);

        // $this->id_lingkup=htmlspecialchars(strip_tags($this->id_lingkup));
        // $this->usia=htmlspecialchars(strip_tags($this->usia));
        // $this->interval=htmlspecialchars(strip_tags($this->interval));

        // $stmt->bindparam(":lingkup", $this->id_lingkup);
        // $stmt->bindparam(":usia", $this->usia);
        // $stmt->bindparam(":interval", $this->interval);

        $stmt->execute();

        return $stmt;
    }
}

?>