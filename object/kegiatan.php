<?php
    class Kegiatan{
        private $conn;
        private $table_name = "kegiatan";

        //object properties 
        public $id_kegiatan;
        public $kegiatan;
        public $parent;

        // constructor with $db as database connection
        public function __construct($db){
            $this->conn = $db;
        }

        public function read()
        {
            $query = "SELECT * FROM ".$this->table_name;

            $stmt = $this->conn->prepare($query);

            $stmt->execute();

            return $stmt;
        }
    }
?>