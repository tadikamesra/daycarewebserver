<?php
class Lingkup{
    private $conn;
    private $table_name = "lingkup_perkembangan";

    //object properties 
    public $id_lingkup;
    public $lingkup;
    public $usia;
    public $interval;
    public $parent;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    public function read()
    {
        $query = "SELECT * FROM ".$this->table_name;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }
}

?>