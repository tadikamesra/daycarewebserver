<?php
class Ortu{
    private $conn;
    private $table_name = "orangtua";

    //object properties 
    public $id_ortu;
    public $nik;
    public $nama;
    public $jk;
    public $telp;
    public $alamat;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    public function read()
    {
        $query = "SELECT * FROM ".$this->table_name;

        $stmt = $this->conn->prepare($query);

        $stmt->execute();

        return $stmt;
    }

    public function add(){
        $query = "INSERT INTO ".$this->table_name." SET 
                        nama=:nama,
                        nik=:nik,
                        jk=:jk,
                        telp=:telp,
                        alamat=:alamat";
            
            $stmt = $this->conn->prepare($query);

            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->nik=htmlspecialchars(strip_tags($this->nik));
            $this->jk=htmlspecialchars(strip_tags($this->jk));
            $this->telp=htmlspecialchars(strip_tags($this->telp));
            $this->alamat=htmlspecialchars(strip_tags($this->alamat));

            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":nik", $this->nik);
            $stmt->bindParam(":jk", $this->jk);
            $stmt->bindParam(":telp", $this->telp);
            $stmt->bindParam(":alamat", $this->alamat);

            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
    }

    public function update(){
        $query = "UPDATE ".$this->table_name." SET 
                        nama=:nama,
                        nik=:nik,
                        jk=:jk,
                        telp=:telp,
                        alamat=:alamat,
                        last_update=now()
                        WHERE id_ortu=:id_ortu";
            
            $stmt = $this->conn->prepare($query);

            $this->id_ortu=htmlspecialchars(strip_tags($this->id_ortu));
            $this->nama=htmlspecialchars(strip_tags($this->nama));
            $this->nik=htmlspecialchars(strip_tags($this->nik));
            $this->jk=htmlspecialchars(strip_tags($this->jk));
            $this->telp=htmlspecialchars(strip_tags($this->telp));
            $this->alamat=htmlspecialchars(strip_tags($this->alamat));

            $stmt->bindParam(":id_ortu", $this->id_ortu);
            $stmt->bindParam(":nama", $this->nama);
            $stmt->bindParam(":nik", $this->nik);
            $stmt->bindParam(":jk", $this->jk);
            $stmt->bindParam(":telp", $this->telp);
            $stmt->bindParam(":alamat", $this->alamat);

            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
    }

    public function login(){
        $query = "SELECT * FROM ". $this->table_name ." 
        WHERE nik=:nik AND telp=:telp";

        //prepare query 
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->nik=htmlspecialchars(strip_tags($this->nik));
        $this->telp=htmlspecialchars(strip_tags($this->telp));

        // bind given nik value
        $stmt->bindParam(":nik", $this->nik);
        $stmt->bindParam(":telp", $this->telp);

        $stmt->execute();

        return $stmt;
    }
    
    // check if given nik exist in the database
    function nikExists(){        
        // query to check if nik exists
        $query = "SELECT * FROM " . $this->table_name . "
                WHERE nik=:nik LIMIT 0,1";
    
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    
        // sanitize
        $this->nik=htmlspecialchars(strip_tags($this->nik));
    
        // bind given nik value
        $stmt->bindParam(':nik', $this->nik);
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // if nik exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
            // return true because nik exists in the database
            return true;
        }        
        // return false if nik does not exist in the database
        return false;
    }
}
?>