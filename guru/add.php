<?php
// required headers
    //header("Access-Control-Allow-Origin: http://localhost/EZPrint/admin/api/");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 

    // include database and object files
    include_once '../config/database.php';
    include_once '../object/guru.php';

    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();

    // initialize object
    $guru = new Guru($db);
    $response = array();

    if (isset($_POST['nama']) 
        && isset($_POST['nip']) 
        && isset($_POST['email'])
        && isset($_POST['jk'])
        && isset($_POST['telp'])
        && isset($_POST['password'])) {
        $guru->nama = htmlspecialchars($_POST['nama']);
        $guru->nip = htmlspecialchars($_POST['nip']);
        $guru->email = htmlspecialchars($_POST['email']);
        $guru->jk = htmlspecialchars($_POST['jk']);
        $guru->telp = htmlspecialchars($_POST['telp']);
        $guru->password = md5(htmlspecialchars($_POST['password']));

        $stmt = $guru->add();

        if ($stmt) {
            $response['error'] = false;
            $response['message'] = 'Insert data success';
            // set response code - 200 OK
            http_response_code(200);    
            // show products data in json format
            echo json_encode($response);
        }else{
            // set response code - 502 Bad Gateway
            http_response_code(502);
            $response['error'] = true;
            $response['message'] = 'Insert data failed';    
            // show products data in json format
            echo json_encode($response);
        }
        
    } else {
        // set response code - 400 Bad Request
        http_response_code(400);    
        $response['error'] = true;
        $response['message'] = 'Required parameters are not available';
        // show products data in json format
        echo json_encode($response);
    }
?>