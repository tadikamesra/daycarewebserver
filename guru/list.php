<?php
    // required headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    // include database and object files
    include_once '../config/database.php';
    include_once '../object/guru.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize object
    $guru = new Guru($db);

    //query guru
    $stmt = $guru->list();
    $num = $stmt->rowCount();

    //check if more than 0 record found
    if($num > 0){
        //guru array
        $guru_arr = array();
        $guru_arr["guru"] = array();

        // retrieve our table contents
        // fetch() is faster than fetchAll()
        // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);
    
            $guru_item=array(
                "id_guru" => $id_guru,
                "nama" => $nama,
                "nip" => $nip,
                "email" => $email,
                "jk" => $jk,
                "telp" => $telp,
                "password" => $password
            );
    
            array_push($guru_arr["guru"], $guru_item);
        }
    
        // set response code - 200 OK
        http_response_code(200);
    
        // show products data in json format
        echo json_encode($guru_arr);
    }else{
        // set response code - 404 Not found
        http_response_code(404);
     
        // tell the user no products found
        echo json_encode(
            array("message" => "No guru found.")
        );
    }
?>