<?php
    // required headers
    //header("Access-Control-Allow-Origin: http://localhost/EZPrint/admin/api/");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 

    // include database and object files
    include_once '../config/database.php';
    include_once '../object/guru.php';
    
    // instantiate database and product object
    $database = new Database();
    $db = $database->getConnection();

    //initialize object
    $guru = new Guru($db);
    $guru_arr = array();

    if (isset($_POST['email']) && isset($_POST['password'])) {
        $guru->email = htmlspecialchars($_POST['email']);
        $guru->password = md5(htmlspecialchars($_POST['password']));                

        // Check jika email sudah terdaftar
        if($guru->emailExists()){
            $stmt = $guru->login();
            $num = $stmt->rowCount();

            //Check jika email dan password cocok
            if($num > 0){
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row);
                    
                    $guru_arr["id_guru"] = $id_guru;
                    $guru_arr["nama"] = $nama;
                    $guru_arr["nip"] = $nip;
                    $guru_arr["jk"] = $jk;
                    $guru_arr["email"] = $email;
                    $guru_arr["telp"] = $telp;
                    $guru_arr["password"] = $password;
                    $guru_arr["previlege"] = $previlege;
                    $guru_arr["error"] = FALSE;
                    $guru_arr["message"] = "Login sukses!";
                }
        
                // set response code - 200 OK
                http_response_code(200);
            
                // show products data in json format
                echo json_encode($guru_arr);
            }else{
                // set response code - 403 Forbidden
                http_response_code(403);

                $guru_arr["error"] = TRUE;
                $guru_arr["message"] = "Password salah!";
        
                // tell the guru no products found
                echo json_encode($guru_arr);    
            }
        }else{            
            // set response code - 404 Not found
            http_response_code(404);
            
            $guru_arr["error"] = TRUE;
            $guru_arr["message"] = "Email belum terdaftar!";

            // tell the guru no products found
            echo json_encode($guru_arr);
        }
    }  
?>