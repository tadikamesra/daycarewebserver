<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/database.php';
include_once '../object/tppa.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

// initialize object
$tppa = new Tppa($db);

//check if more than 0 record found
if (isset($_POST['id_lingkup']) && isset($_POST['usia'])) {
    //$tppa->id_lingkup = htmlspecialchars($_POST['id_lingkup']);
    $lingkup = htmlspecialchars($_POST['id_lingkup']);
    $usia = htmlspecialchars($_POST['usia']);
    //$max = htmlspecialchars($_POST['max']);

    //query tppa
    $stmt = $tppa->getTppa($lingkup, $usia);
    //$stmt = $tppa->read();
    $num = $stmt->rowCount();
    //check if more than 0 record found
    if($num > 0){
        //tppa array
        $tppa_arr = array();
        $tppa_arr["tppa"] = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // extract row
            // this will make $row['name'] to
            // just $name only
            extract($row);

            $tppa_item=array(
                "id_tppa" => $id_tppa,
                "usia" => $usia,
                "interval" => $interval,
                "keterangan" => ucfirst($keterangan),
                "id_lingkup" => $id_lingkup
            );

            array_push($tppa_arr["tppa"], $tppa_item);
        }

        // set response code - 200 OK
        http_response_code(200);

        // show products data in json format
        echo json_encode($tppa_arr);    
    }else{

        // set response code - 404 Not found
        http_response_code(404);
    
        // tell the user no products found
        echo json_encode(
            array("message" => "No file found.")
        );
    }
}else{
        // set response code - 400 Bad Request
        http_response_code(400);    
        $response['error'] = true;
        $response['message'] = 'Required parameters are not available';
        // show products data in json format
        echo json_encode($response);
}
?>